<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\AddToCart;
use App\Category;
use App\Product;
use DB;

class AddToCartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::where('publication_status', 1)
                ->take(6)
                ->get();
        return view('shipping');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
        return view('cart');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
 {
        if($request->size!=NULL){
            $product = Product::where('id', $request->product_id)->first();
        $addTocart = AddToCart::where('product_id', $request->product_id)->where('session_id', Session::getId())->first();
        if ($request->publication_status == 2) {
            Session::flash('message', 'Your Selected Product Already Stock Out..!');
            return Redirect::to('/product-details/' . $request->product_id);
        } elseif ($addTocart != NULL && $request->product_quantity > 0 && $request->product_quantity <= $product->product_quantity) {
            $qty = $request->product_quantity + $addTocart->product_quantity;
            $cart = AddToCart::where('product_id', $request->product_id)->where('session_id', Session::getId())->update(['product_quantity' => $qty]);
            Session::flash('message', 'Your Selected Product Has Been Saved Cart!');
            return Redirect::to('/product-details/' . $request->product_id);
        } elseif ($request->product_quantity > 0 && $request->product_quantity <= $product->product_quantity) {
            $AddToCart = new AddToCart;
            $AddToCart->product_id = $request->product_id;
            $AddToCart->product_name = $request->product_name;
            $AddToCart->product_name_bn = $request->product_name_bn;
            $AddToCart->product_code = $request->product_code;
            $AddToCart->product_price = $request->product_price;
            $AddToCart->product_quantity = $request->product_quantity;
            $AddToCart->size = $request->size;
            $AddToCart->session_id = Session::getId();
            if ($AddToCart->save()) {
                Session::flash('message', 'Your Selected Product Has Been Saved Cart!');
                return Redirect::to('/product-details/' . $request->product_id);
            }
        } else {
            Session::flash('message', 'Your selected product Qty is not available!');
            return Redirect::to('/product-details/' . $request->product_id);
        }
      //---------------------------Not use Size ------------------------
        }else{

           $product = Product::where('id', $request->product_id)->first();
        $addTocart = AddToCart::where('product_id', $request->product_id)->where('session_id', Session::getId())->first();
        if ($request->publication_status == 2) {
            Session::flash('message', 'Your Selected Product Already Stock Out..!');
            return Redirect::to('/product-details/' . $request->product_id);
        } elseif ($addTocart != NULL && $request->product_quantity > 0 && $request->product_quantity <= $product->product_quantity) {
            $qty = $request->product_quantity + $addTocart->product_quantity;
            $cart = AddToCart::where('product_id', $request->product_id)->where('session_id', Session::getId())->update(['product_quantity' => $qty]);
            Session::flash('message', 'Your Selected Product Has Been Saved Cart!');
            return Redirect::to('/product-details/' . $request->product_id);
        } elseif ($request->product_quantity > 0 && $request->product_quantity <= $product->product_quantity) {
            $AddToCart = new AddToCart;
            $AddToCart->product_id = $request->product_id;
            $AddToCart->product_name = $request->product_name;
            $AddToCart->product_name_bn = $request->product_name_bn;
            $AddToCart->product_code = $request->product_code;
            $AddToCart->product_price = $request->product_price;
            $AddToCart->product_quantity = $request->product_quantity;
            $AddToCart->size = "None";
            $AddToCart->session_id = Session::getId();
            if ($AddToCart->save()) {
                Session::flash('message', 'Your Selected Product Has Been Saved Cart!');
                return Redirect::to('/product-details/' . $request->product_id);
            }
        } else {
            Session::flash('message', 'Your selected product Qty is not available!');
            return Redirect::to('/product-details/' . $request->product_id);
        }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function SaveOrder(Request $request){
          
      }
      
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AddToCart::where('product_id',$id)->where('session_id',Session::getId())->delete();
        Session::flash('message', 'Your Selected Cart Product Has Been Removed Successfully ....!');
            return Redirect::to('/shipping');
    }
}

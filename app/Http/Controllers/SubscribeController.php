<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Session;
use App\Subscribe;
use DB;

class SubscribeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.subscribes_user');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function gjson()
    {
        $subscribe = DB::table('subscribes')->first();
        return response()->json($subscribe);
    }

    public function gjsons()
    {
       
        return view('show')->with('info', json_decode(email, true));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,array(
           'email'=>'required|max:50'
       ));

        $subscribeInfo = DB::table('subscribes')->where('email',$request->email)->first();
        if($subscribeInfo!=NULL){
Session::flash('message','This User Already subscribed ..!');
        return Redirect::to('/');
        }else{
          $subscribe = new Subscribe;
      
       $subscribe->email = $request->email;
     
      
       $subscribe->save();
           Session::flash('message','You have been subscribed successfully..!');
        return Redirect::to('/');
       
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function destroy($id)
    {
        DB::table('subscribes')->where('id',$id)->delete();
        Session::flash('message', 'Your Selected user Has Been Deleted Successfully ....!');
            return Redirect::to('/show-subscribe');
    }
    
    
    
     public function show_message()
    {
        $messages = DB::table('contacts')->get();
        
        return view('admin.pages.messages',compact('messages'));
    }

}

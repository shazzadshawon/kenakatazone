<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Response;
use Redirect;
use Session;
use App\Category;
use App\SubCategory;
use App\Product;
use App\ProductImage;
use App\product_size;
use DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $product = DB::table('products')
                ->join('categories', 'products.category_id', '=', 'categories.category_id')      
                 ->join('sub_categories', 'products.sub_category_id', '=', 'sub_categories.sub_category_id')      
                ->select('products.*', 'categories.category_name','sub_categories.sub_category_name')
                ->get();
        return view('admin.pages.manage_product')->with('Product',$product);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('publication_status',1)->get();
        $sub_categories = SubCategory::where('publication_status',1)->get();

        return view('admin.pages.add_product')->withCategories($categories)->withSubCategories($sub_categories);
    }
    
     public function SubCategory($id)
    {
        $sub_categories = SubCategory::where('category_id',$id)->get();

        return view('admin.pages.select_sub_categories')->withSubCategories($sub_categories);
    }
    
 public function AddProductImage(Request $request, $id) {
        $files = $request->file('product_image');
      // Making counting of uploaded images
      $file_count = count($files);
      // start count how many uploaded
      $uploadcount = 0;

      foreach ($files as $file) {
        $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
        $validator = Validator::make(array('file'=> $file), $rules);
        if($validator->passes()){
          $destinationPath = 'product_image/'; // upload folder in public directory
          $filename = $file->getClientOriginalName();
          $upload_success = $file->move($destinationPath, $filename);
          $uploadcount ++;

          // save into database
          $extension = $file->getClientOriginalExtension();
          $entry = new ProductImage();          
          $entry->product_image = $upload_success;
          $entry->product_id = $id;
          $entry->save();
        }
      }
      if($uploadcount == $file_count){
        Session::flash('message', 'Your Product Images Has Been Uploaded successfully...!');
        return Redirect::to('/product/'.$id);
      } else {
          Session::flash('message', 'Your Product Images Has Been Uploaded successfully...!');
      return Redirect::to('/product/'.$id);
      }
    }

    public function AddProductSize(Request $request, $id) {      

          $product_size = new product_size();
          $product_size->product_id = $id;
          $product_size->size = $request->size;       
     
      
      if($product_size->save()){
        Session::flash('message', 'Your Product Size Has Been Uploaded successfully...!');
        return Redirect::to('/product/'.$id);
      } else {
          Session::flash('message', 'Your Product Size Has Been not Uploaded ...!');
      return Redirect::to('/product/'.$id);
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,array(
           'category_id'=>'required|max:255',
           'product_image'=>'required'
       ));
        $Product = new Product();
        $Product->category_id = $request->category_id;
        $Product->sub_category_id = $request->sub_category_id;
        $Product->sub_sub_category_id = $request->sub_sub_category_id;
        $Product->product_name = $request->product_name;
        $Product->product_name_bn = $request->product_name_bn;
        $Product->product_code = $request->product_code;
        $Product->product_price = $request->product_price;
        $Product->product_quantity = $request->product_quantity;
        $Product->discount = $request->discount;
        $Product->description = $request->description;
        $Product->description_bn = $request->description_bn;
        $Product->offer_status = $request->offer_status;
        $Product->publication_status = $request->publication_status;  
 
           if($Product->save()){
        $files = $request->file('product_image');
      // Making counting of uploaded images
      $file_count = count($files);
      // start count how many uploaded
      $uploadcount = 0;

      foreach ($files as $file) {
        $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
        $validator = Validator::make(array('file'=> $file), $rules);
            if($validator->passes()){
              $destinationPath = 'product_image/'; // upload folder in public directory
              $filename = $file->getClientOriginalName();
              $upload_success = $file->move($destinationPath, $filename);
              $uploadcount ++;

              // save
              $extension = $file->getClientOriginalExtension();
              $product_image = new ProductImage();      
              $product_image->product_id =  $Product->id;
              $product_image->product_image = $upload_success;         
              $product_image->save();
            }
        }
         if($uploadcount == $file_count){
        Session::flash('message', 'Your Product Information Has Been Uploaded successfully...!');
        return Redirect::to('/add-product');
      } else {
          Session::flash('message', 'Product Has Ben Not Uploaded! Please Input Valid Data ..');
        return Redirect::to('/add-product');
      }
           }  else {
                 Session::flash('message', '');
        return Redirect::to('/add-product');
           }
    }

          public function unpublished($id) {

         $product = Product::where('id',$id)
                ->update(['publication_status' => 0]);
        Session::flash('message', 'Your Selected Product Has Been Unpublished Successfully..!');
        return Redirect::to('/manage-product');
    }
    
      public function published( $id)
    {

        $product = Product::where('id',$id)
                ->update(['publication_status' =>1]);

            Session::flash('message', 'Your Selected Product Has Been published Successfully..!');
            return Redirect::to('/manage-product');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $product = DB::table('products')->where('id',$id)
                ->join('categories', 'products.category_id', '=', 'categories.category_id')      
                 ->join('sub_categories', 'products.sub_category_id', '=', 'sub_categories.sub_category_id')      
                ->select('products.*', 'categories.category_name','sub_categories.sub_category_name')
                ->first();
         $ProductImage=DB::table('product_images')->where('product_id',$id)->get();
        return view('admin.pages.view_product')->with('product_info',$product)->with('image',$ProductImage);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $products = Product::find($id);
        $categories = Category::where('publication_status', 1)->get();
        $sub_categories = SubCategory::where('publication_status', 1)->get();
        return view('admin.pages.edit_product')->withProducts($products)->withCategories($categories)->withSubCategories($sub_categories);
    }
//     public function EditProductImage($id) {
//      $product_image = ProductImage::where('product_image_id',$id)->first();
//       return view('admin.pages.edit_product_image')->withProductImage($product_image);
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::where('id', $id)
                ->update([
            'category_id' => $request->category_id,
            'sub_category_id' => $request->sub_category_id,
            'product_name' => $request->product_name,
            'product_name_bn' => $request->product_name_bn,
            'product_code' => $request->product_code,
            'product_price' => $request->product_price,
            'product_quantity' => $request->product_quantity,
            'discount' => $request->discount,
            'description' => $request->description,
            'description_bn' => $request->description_bn,
            'offer_status' => $request->offer_status,
            'publication_status' => $request->publication_status
        ]);
        Session::flash('message', ' Product Information Has Been Updated Successfully..!');
            return Redirect::to('/manage-product');
    }
    
//        public function UpdateProductImage(Request $request, $product_id,$product_image_id)
//    {
//
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::where('id',$id)->delete();
        ProductImage::where('product_id',$id)->delete();
        Session::flash('message', 'Your Selected Product Has Been Deleted Successfully ....!');
            return Redirect::to('/manage-product');
    }
    
    public function DeleteProductImage($product_id,$product_image_id)
    {
       
        ProductImage::where('product_image_id',$product_image_id)->delete();
        Session::flash('message', 'Your Selected Product Image Has Been Deleted Successfully ....!');
            return Redirect::to('/product/'.$product_id);
    }

    public function DeleteProductSize($product_id,$id)
    {
       
        product_size::where('id',$id)->delete();
        Session::flash('message', 'Your Selected Product Image Has Been Deleted Successfully ....!');
            return Redirect::to('/product/'.$product_id);
    }
    
    public function myformAjax($id)
    {
        $cities = DB::table("sub_categories")
                    ->where("category_id",$id);
                   
        return json_encode($cities);
    }
}

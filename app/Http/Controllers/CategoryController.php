<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Category;

use DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('admin.pages.manage_category')->withCategories($categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.add_category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
           'category_name'=>'required|max:255'
       ));
        $category = new Category;
      
       $category->category_name = $request->category_name;
       $category->category_name_bn = $request->category_name_bn;
       $category->publication_status = $request->publication_status;
      
       if($category->save()){
           Session::flash('message','Category has been Saved Successfully ....!');
        return Redirect::to('/add-category');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         // find the post in the database and save as a var
        $category = Category::where('category_id',$id)->first();
        // return the view and pass in the var we previously created
        return view('admin.pages.edit_category')->withCategory($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request,array(
           'category_name'=>'required|max:255'          
       ));
//        $category = new Category;
        $category = Category::where('category_id',$id)
                ->update([
                    'category_name' => $request->category_name,
                    'category_name_bn' => $request->category_name_bn,
                    'publication_status' =>$request->publication_status
                        ]);
//        $category = Category::find($category_id);
//        $category->category_name = $request->category_name;
//        $category->publication_status = $request->publication_status;
//        $category->save();
      
        
            Session::flash('message', 'Category Has Been Updated Successfully..!');
            return Redirect::to('/manage-category');
    }
      public function unpublished($id) {

//        $category = new Category;
        $category = Category::where('category_id', $id)
                ->update(['publication_status' => 0]);
//        $category = Category::find($category_id);
//        $category->category_name = $request->category_name;
//        $category->publication_status = $request->publication_status;
//        $category->save();


        Session::flash('message', 'Category Has Been Unpublished Successfully..!');
        return Redirect::to('/manage-category');
    }
    
      public function published( $id)
    {
   
//        $category = new Category;
        $category = Category::where('category_id',$id)
                ->update(['publication_status' =>1]);
//        $category = Category::find($category_id);
//        $category->category_name = $request->category_name;
//        $category->publication_status = $request->publication_status;
//        $category->save();
      
        
            Session::flash('message', 'Category Has Been published Successfully..!');
            return Redirect::to('/manage-category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//       DB::table('categories')->where('category_id',$id) ->delete(); 
        Category::where('category_id',$id)->delete();
        Session::flash('message', 'Your Selected Category Has Been Deleted Successfully ....!');
            return Redirect::to('/manage-category');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('product_id');
             $table->integer('customer_id');
            $table->string('product_name', 200);
            $table->string('product_code', 200);
            $table->string('product_price', 200);
            $table->string('product_quantity', 45);
            $table->string('size', 45);
            $table->string('order_number', 255);
            $table->string('session_id', 255);
             $table->tinyInteger('publication_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

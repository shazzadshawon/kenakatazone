<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);

          DB::table('users')->insert([
             'name' => 'TheEazyBuy',
             'email' => 'info@theeazybuy.com',
             'password' => bcrypt('Pr4-GSO@ID*a'),
         ]);
    }
}

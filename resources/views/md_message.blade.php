
<!DOCTYPE html>
<html class=""><!--<![endif]-->
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">

    <title>ArtandCreativeinterior</title>

    <!-- Standard Favicon -->
    <link rel="icon" type="image/x-icon" href="../icon/LOGO.jpg" />
    
    <!-- For iPhone 4 Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images//apple-touch-icon-114x114-precomposed.png">
    
    <!-- For iPad: -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images//apple-touch-icon-72x72-precomposed.png">
    
    <!-- For iPhone: -->
    <link rel="apple-touch-icon-precomposed" href="images//apple-touch-icon-57x57-precomposed.png"> 
    
    <!-- Custom - Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('style.css') }}">
    
    <!--[if lt IE 9]>
        <script src="js/html5/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .photo-slider .item {
    padding: 10px 0;
}
.carousel-caption {
  
    padding-bottom: 0px;
}
.middle-header {
    margin-top: 5px;
    margin-bottom: 10px;
}

#figure:hover img{
  -webkit-transform: scale3d(1.2, 1.2, 1);
          transform: scale3d(1.2, 1.2, 1);
}

.style_prevu_kit:hover
{
    box-shadow: 0px 0px 150px #000000;
    z-index: 2;
    -webkit-transition: all 200ms ease-in;
    -webkit-transform: scale(1.5);
    -ms-transition: all 200ms ease-in;
    -ms-transform: scale(1.5);   
    -moz-transition: all 200ms ease-in;
    -moz-transform: scale(1.5);
    transition: all 200ms ease-in;
    transform: scale(1.5);
    overflow: hidden;
}
    </style>
    
</head>

<body data-offset="200" data-spy="scroll" data-target=".ow-navigation">

    
    
    <!-- Header -->
    @include('pages.header')
    <!-- Header /- -->
    
    <main class="site-main page-spacing">
        <!-- Page Banner -->
        <div class="page-banner container-fluid no-padding">
            <div class="page-banner-content">
                <h3>MD Message</h3>
            </div>
        </div><!-- Page Banner /- -->
        
        <!-- Contact Us -->
        <div class="contact-us container-fluid no-padding">
            <!-- Map Section -->
            <!--  Map Section /- -->
            <div class="section-padding"></div>
            
            <!-- Container -->
            <div class="container">
                 <div class="col-md-6 col-sm-6 col-xs-6 contact-form">
                   <img src="../icon/md.JPG">
                </div> 
                
                <div class="col-md-6 col-sm-12 col-xs-12 contact-detail">
                    <!-- Section Header -->
                    <div class="section-header">
                        <h3>Managing Directors message:</h3>
                        <p style="color: black; font-size: 16px;">
                        I take this opportunity to thank our valued customers, whose continued patronage and confidence in our products inspires us to extend the best of services and enables us to provide value for their money. <br><br>
                            
                            Being dedicated to taking furniture’s & Interiors to rural areas, we are focused at addressing the needs of our customers through rugged, efficient, reliable and economic and automation solutions and products, in line with the world’s best, while maintaining continuous interaction with them to assess their emerging requirement, so as to be ready when the needs arise.<br><br>

                            We believe that technology holds the key to reliability, safty, Flexibility and, which are necessary for life easy.<br><br>
                            We are committed to total customer satisfaction by identifying their specific needs, translating them into Quality products and providing dependable after-sales-services. This commitment is the corner stone of our Quality Policy and we strive to achieve it by putting into place a Quality System. <br><Br>


We plan to achieve this goal through our strength - the Employees; and seek their continuous involvement in achieving the Company's objectives.<br>

The organization is also committed to its shareholders by way of maximizing the wealth through sustained growth under the overall ambit of the spirit of a Public Sector Unit, to optimally balance the commercial objectives and the goals of social service to the nation at large.<Br>

I therefore, seek continued patronage of our valued customers, cooperation of our employees and thank our well-wishers who have contributed to the growth of the organization.<br>



</p>
                        
                    </div><!-- Section Header /- -->
                    {{-- <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6 contact-content">
                            <div class="contact-info">
                                <span class="icon icon-Pointer"></span>
                                <p>09 Downtown, Store Main St,Victoria, Australia</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 contact-content">
                            <div class="contact-info">
                                <span class="icon icon-Mail"></span>
                                <p><a href="mailto:info@ourdomain.com" title="Info@ourdomain.com">Info@ourdomain.com</a></p>
                                <p><a href="mailto:xyz@ourdomain.com" title="Support@ourdomain.com">Support@ourdomain.com</a></p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 contact-content">
                            <div class="contact-info">
                                <span class="icon icon-Phone"></span>
                                <p>09 Downtown, Store Main St,Victoria, Australia</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 contact-content">
                            <div class="contact-info">
                                <span class="icon icon-Phone"></span>
                                <p>Free standard shipping on all orders</p>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div><!-- Container /- -->
            <div class="section-padding"></div>
        </div><!-- Contact Us /- -->
    </main>
    
 @include('pages.footer')
    
    
    
    <!-- JQuery v1.11.3 -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>

    <!-- Library - Js -->
    <script src="{{ asset('libraries/lib.js') }}"></script>
    <!-- Bootstrap JS File v3.3.5 -->
    
    <script src="{{ asset('libraries/jquery.countdown.min.js') }}"></script>
    
    <script src="{{ asset('libraries/lightslider-master/lightslider.js') }}"></script>
    
    <script src="{{ asset('libraries/slick/slick.min.js') }}"></script>

    <!-- Library - Google Map API -->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    
    <!-- Library - Theme JS -->
    <script src="{{ asset('js/functions.js') }}"></script>
</body>
</html>
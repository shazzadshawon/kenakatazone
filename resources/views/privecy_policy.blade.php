<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/bootstrap/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/select2/css/select2.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/jquery.bxslider/jquery.bxslider.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/owl.carousel/owl.carousel.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/jquery-ui/jquery-ui.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/animate.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/reset.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/responsive.css') }}" />
    
    <title>Kenakatazone</title>
</head>
<body class="category-page">
<!-- HEADER -->
@include('pages.menu2')
<!-- end header -->
@if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3 style="color: green; text-align: center;"> {{Session::get('message')}}</h3>
</div>
      
@endif
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">
                @if (Session::has('EN'))
                 Privecy Policy
                @else
                    প্রাইভেসি পলিসি
                @endif
            </span>
        </h2>
        <!-- ../page heading-->
        <div id="contact" class="page-content page-contact">
            <div id="message-box-conact"></div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>
                            @if (Session::has('EN'))
                                Refund Policy
                            @else
                                 রিফান্ড পলিসি
                            @endif
                            </h2>
                        </div>
                        <div class="panel-body">
                            @if (Session::has('EN'))
                                You can get back the full value of the purchased product for any reason after accepting the product. In the case of maximum 72 hours after receiving the product, you need to email to complain@kenakatazone.com or call us on Hotline number 01707744499 and we will have to inform. Note that in order to withdraw the full refund of the product, the product must be fully intact / trough-free and you have to bear all the transportation costs for returning.
                            @else
                                পণ্য গ্রহনের পর যে কোন কারনে ক্রয়কৃত পণ্যের সম্পূর্ণ মূল্য আপনি ফেরত পেতে পারেন। সেক্ষেত্রে পণ্য গ্রহনের পর সর্বোচ্চ 72 ঘণ্টার মধ্যে আপনাকে complain@kenakatazone.com এ ই মেইল করতে হবে অথবা আমাদের হটলাইন নাম্বার 01707744499 এ কল করে আমাদের অবহিত করতে হবে। উল্লেখ্য যে পণ্যের পুরো টাকা ফেরত নেবার ক্ষেত্রে পণ্যটি সম্পূর্ণ অক্ষত/ ত্রূটিমুক্ত অবস্থায় থাকতে হবে এবং ফেরত পাঠানোর সকল পরিবহন খরচ আপনাকে বহন করতে হবে।
                            @endif
                        </div>
                        <div class="panel-footer">
                            <a href="" class="btn btn-warning">
                                <i class="fa fa-question fa-lg" aria-hidden="true"></i>
                            @if (Session::has('EN'))
                            Question
                            @else
                            প্রশ্ন
                            @endif
                            </a>
                            <a href="" class="btn btn-success">
                                 <i class="fa fa-ellipsis-h fa-lg" aria-hidden="true"></i>
                            @if (Session::has('EN'))
                            Opinion
                            @else
                            মতামত
                            @endif
                            </a>
                            <a href="" class="btn btn-primary">
                                <i class="fa fa-edit fa-lg" aria-hidden="true"></i>
                            @if (Session::has('EN'))
                            Complaint
                            @else
                            অভিযোগ
                            @endif
                            </a>
                        </div>
                    </div>

                    
                </div>
            </div>
            
        </div>
    </div>
</div>
                    
                


<!-- Footer -->
@include('pages.footer')

<a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a>
<!-- Script-->
<script type="text/javascript" src="{{ asset('assets/lib/jquery/jquery-1.11.2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery.bxslider/jquery.bxslider.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/owl.carousel/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery.countdown/jquery.countdown.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/lib/jquery-ui/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.actual.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/js/theme-script.js') }}"></script>

</body>
</html>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/bootstrap/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/select2/css/select2.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/jquery.bxslider/jquery.bxslider.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/owl.carousel/owl.carousel.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/jquery-ui/jquery-ui.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/animate.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/reset.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/responsive.css') }}" />
    
    <title>Kenakatazone</title>
</head>
<body class="category-page">
<!-- HEADER -->
@include('pages.menu2')
<!-- end header -->
@if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3 style="color: green; text-align: center;"> {{Session::get('message')}}</h3>
</div>
      
@endif
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
       
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
           
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
               
               
                     <h1>
                                            @if (Session::has('EN'))
                                             About Us
                                            @else
                                             এবাউট আস
                                            @endif
                     </h1> 
                                            @if (Session::has('EN'))
                                            <p>
                                                KenakataZone is an online shopping & e-commerce site which was established on 1st January 2017 by a group of an entrepreneur. It offers a wide range of product which includes fashion, beauty, jewelry, shoes, electronics, home appearance, stationary, books, kids’ appearance and much more at a fair price. Home delivery service is available for all sorts of products. We offer various payment options including cash on delivery. KenakataZone let you enjoy an awesome shopping experience with your order sent directly to your doorstep. We are constantly updating ours.
                                            </p>
                                            @else
                                            <br>
                                            <p>
                                              KenakataZone  একটি অনলাইন শপিং এবং ই-কমার্স সাইট যা 1 জানুয়ারি ২017 সালে উদ্যোক্তাদের একটি গ্রুপ দ্বারা প্রতিষ্ঠিত হয়। এটি একটি বিস্তৃত পণ্য সরবরাহ করে যা ফ্যাশন, সৌন্দর্য, জুয়েলারী, জুতা, ইলেকট্রনিক্স, বাড়ির উপস্থিতি, স্থিরীকৃত, বই, বাচ্চাদের উপস্থিতি প্রভৃতি পণ্য গুলি ন্যায্য মূল্যের মধ্যে আছে। হোম ডেলিভারি সেবা সব ধরণের পণ্য গুলির জন্য উপলব্ধ। আমরা পণ্য গুলির  উপর নগদ সহ বিভিন্ন পেমেন্ট বিকল্প অফার দিয়ে থাকি  । KenakataZone আপনাকে আপনার অর্ডার সরাসরি আপনার দোরগোড়ায় প্রেরণের  একটি অসাধারণ কেনাকাটা অভিজ্ঞতা  । আমাদের সর্বশেষ বৈশিষ্ট্য সঙ্গে সর্বশেষ পণ্য নিশ্চিত করতে আমাদের পণ্য পরিসীমা ক্রমাগত আপডেট করা হয়। ফেসবুকে আমাদের অনুসরণ করুন এবং সর্বশেষ অফার এবং খবর আপডেট থাকুন। শুভ কেনাকাটা!
                                            @endif
                     
                     <!--<p> KenakataZone is an online shopping & e-commerce site which was established in 1st January, 2017 by a group of entrepreneur. It offers wide range of product which includes fashion, beauty, jewellary, shoes, electronics, home appearance, stationary, books, kids’ appearance and many more at a fair price. Home delivery service is available for all sorts of products. We offer various payment options including cash on delivery. KenakataZone let you enjoy an awesome shopping experience with your order sent directly to your doorstep. We are constantly updating our.</p>-->
                                             
                </div>
                <!-- ./view-product-list-->
      
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>


<!-- Footer -->
@include('pages.footer')

<a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a>
<!-- Script-->
<script type="text/javascript" src="{{ asset('assets/lib/jquery/jquery-1.11.2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery.bxslider/jquery.bxslider.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/owl.carousel/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery.countdown/jquery.countdown.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/lib/jquery-ui/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.actual.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/js/theme-script.js') }}"></script>

</body>
</html>
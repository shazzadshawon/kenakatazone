
@if(isset($_GET['EN'])=="EN")
<?php Session()->put('EN',$_GET['EN']); ?>
@elseif(isset($_GET['BN'])=="BN")
   <?php Session::put('EN',null); ?>
@endif
<div class="top-header">
        <div class="container">
            <div class="nav-top-links">
                <a class="first-item" href="#">
                   @if (Session::has('EN'))
                     Welcome to kenakatazone 
                     @else
                     ওয়েলকাম টু কেনাকাটাজোন
                   @endif
                 
                @php                
                   $link = str_replace("http://localhost:8000" ,"http://localhost:8000/bn" ,url()->current());
                @endphp
          

                    
                </a>
                <a href="#">
                
                  @if (Session::has('EN'))
                       Hot Line: 01707744455, 01707744466             
                  @else
                       হট লাইন: ০১৭০৭৭৪৪৪৫৫, ০১৭০৭৭৪৪৪৬৬
                  @endif
                 </a>
            </div>
           {{--  <a class="btn-fb-login" href="#">Login fb </a> --}}
            <div class="support-link">
               
                <form method="get" action="{{ url()->current() }}">
                {{--  <input type="hidden" name="EN" value="EN"> --}}
                <button type="submit" class="btn btn-primary" name="EN" value="EN">EN</button>
                 <button type="submit" class="btn btn-success" name="BN" value="BN">BN</button>
                </form>

              

            </div>
            <div id="user-info-top" class="user-info pull-right">
                <div class="dropdown">
                    <a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><span>
                        @if (Session::has('EN'))
                      Myaccount            
                  @else
                      মাই একাউন্ট
                  @endif
                    </span></a>
                    <ul class="dropdown-menu mega_dropdown" role="menu">
                    <li><a href="{{ URL::to('/User-Register') }}">
                               @if (Session::has('EN'))
                               Login / Register       
                                @else
                               লগইন / রেজিস্টার
                               @endif
                               </a>
                               </li>
                        @if (Session::has('customer_id'))
                        <li><a href="{{URL::to('/logout')}}">
                               @if (Session::has('EN'))
                               LogOut    
                                @else
                               লগ আউট
                               @endif 
                               ({{Session::get('customer_name')}})
                               </a>
                               </li>
                       
                        @endif
                       
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- END MANIN HEADER -->
    <div class="nav-top-menu">
        <div class="container">
            <div class="row">
                <div class="col-sm-3" id="box-vertical-megamenus">
                    <div class="box-vertical-megamenus">
                    <h4 class="title">
                        <span class="title-menu">
                                @if (Session::has('EN'))
                                Categories
                                @else
                                ক্যাটাগরি
                                @endif
                        </span>
                        <span class="btn-open-mobile pull-right home-page"><i class="fa fa-bars"></i></span>
                    </h4>
                    <div class="vertical-menu-content is-home">
                        <ul class="vertical-menu-list">
                           {{--  <li><a href="#"><img class="icon-menu" alt="Funky roots" src="assets/data/12.png">Electronics</a></li> --}}

                             @php
                        $mainCategory = DB::table('categories')
                        ->where('publication_status',1)
                        ->get();
                                @endphp

                                @foreach ($mainCategory as $mainCategoryInfo)
                                    {{-- expr --}}
                                
                            <li>
                                <a class="parent" href="{{ URL::to('Main-Category-products/'.$mainCategoryInfo->category_id) }}">
                                @if (Session::has('EN'))
                    {{ $mainCategoryInfo->category_name }}
                                @else
                     {{ $mainCategoryInfo->category_name_bn }}
                                @endif
                                
                                </a>
                                <div class="vertical-dropdown-menu">
                                    <div class="vertical-groups col-sm-12">
                                    @php
                                       $category = DB::table('sub_categories')
                                       ->where('category_id',$mainCategoryInfo->category_id)
                                       ->where('publication_status',1)
                                       ->get();
                                    @endphp
                                    @foreach ($category as $categoryInfo)
                                                                  
                                        <div class="mega-group col-sm-4">
                                          <a href="{{ URL::to('/Category-products/'.$categoryInfo->sub_category_id) }}">  <h4 class="mega-group-header"><span>
                                                @if (Session::has('EN'))
                    {{ $categoryInfo->sub_category_name }}
                                @else
                     {{ $categoryInfo->sub_category_name_bn }}
                                @endif
                                            </span></h4> </a>
                                            <ul class="group-link-default">

                                               @php
                                       $SubCategory = DB::table('sub_sub_categories')
                                       ->where('sub_category_id',$categoryInfo->sub_category_id)
                                       ->where('publication_status',1)
                                       ->get();
                                    @endphp
                                               @foreach ($SubCategory as $SubCategoryInfo)
                                                <li><a href="{{ URL::to('/Sub-Category-products/'.$SubCategoryInfo->id) }}">
                                 @if (Session::has('EN'))
                    {{ $SubCategoryInfo->sub_sub_category_name }}
                                @else
                     {{ $SubCategoryInfo->sub_sub_category_name_bn }}
                                @endif
                                                </a></li>
                                               @endforeach
                                                
                                            </ul>
                                        </div>

                                    @endforeach
                                      
                                        
                                    </div>
                                </div>
                            </li>
                           
                        @endforeach
                          
                           
                          
        
                        </ul>
                        <div class="all-category"><span class="open-cate">
                            @if (Session::has('EN'))
                               All Categories
                                @else
                              অল  ক্যাটাগরি
                                @endif
                        </span></div>
                    </div>
                </div>
                </div>
      
            </div>
        </div>
    </div>
</div>
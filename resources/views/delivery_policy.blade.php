<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/bootstrap/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/select2/css/select2.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/jquery.bxslider/jquery.bxslider.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/owl.carousel/owl.carousel.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/jquery-ui/jquery-ui.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/animate.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/reset.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/responsive.css') }}" />
    
    <title>Kenakatazone</title>
</head>
<body class="category-page">
<!-- HEADER -->
@include('pages.menu2')
<!-- end header -->
@if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3 style="color: green; text-align: center;"> {{Session::get('message')}}</h3>
</div>
      
@endif
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
       
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
           
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
               
<!--                     <h1>রিফান্ড পলিসিঃ</h1><p style="margin: 30px; font-size: 20px;">-->
<!--পণ্য গ্রহনের পর যে কোন কারনে ক্রয়কৃত পণ্যের সম্পূর্ণ মূল্য আপনি ফেরত পেতে পারেন। সেক্ষেত্রে পণ্য গ্রহনের পর সর্বোচ্চ 72 ঘণ্টার মধ্যে আপনাকে complain@kenakatazone.com এ ই মেইল করতে হবে অথবা আমাদের হটলাইন নাম্বার 01707744499 এ কল করে আমাদের অবহিত করতে হবে। উল্লেখ্য যে পণ্যের পুরো টাকা ফেরত নেবার ক্ষেত্রে পণ্যটি সম্পূর্ণ অক্ষত/ ত্রূটিমুক্ত অবস্থায় থাকতে হবে এবং ফেরত পাঠানোর সকল পরিবহন খরচ আপনাকে বহন করতে হবে।-->
<!-- প্রশ্ন মতামত অভিযোগ-->
<!--ফোন:01707744499ইমেইল: info@kenakatazone.com, complain@kenakatazonel.com    ইনবক্স: https://www.facebook.com/kenakatazone</p>-->

<!--                     <h1>-->
                                            @if (Session::has('EN'))
                                            <h1> Product Change Process:</h1><p style="margin: 30px; font-size: 20px;">
                                            @else
                                             <h1>পণ্য পরিবর্তন প্রক্</h1><p style="margin: 30px; font-size: 20px;">
                                            @endif
                     </h1> 
                                            @if (Session::has('EN'))
                                            <p>
                                               
After accepting the product, you will be able to accept the modified products in case of any problems in the product (eg, broken, torn, not working on the product, the product does not match the product, etc.). In the case of maximum 72 hours after receiving the product, you need to email to complain@kenakatazone.com or call us at our hotline number 01707744499. Note that all transportation costs related to kenakatazone.com will be fully conveyed.
  Question Opinion Complaint
Phone: 01707744499 
Email: info@kenakatazone.com, complain@kenakatazone.com
Inbox: https://www.facebook.com/Kenakatazonecom-627558324114668/
                                            </p>
                                            @else
                                            
                                            <p>রিয়াঃ
পণ্য গ্রহনের পরে আপনি পণ্যের যে কোন সমস্যায় (যেমন: পণ্য ভাঙ্গা, ছেঁড়া, পণ্য কাজ না করা, ছবির সাথে পণ্যের মিল না থাকা ইত্যাদি) ক্ষেত্রে আপনি পরিবর্তিত পণ্য গ্রহণ করতে পারবেন। সেক্ষেত্রে পণ্য গ্রহনের পর সর্বোচ্চ 72 ঘণ্টার মধ্যে আপনি complain@kenakatazone.com এ ই মেইল করতে হবে অথবা আমাদের হটলাইন নাম্বার 01707744499 এ কল করে আমাদের অবহিত করতে হবে। উল্লেখ্য যে এ সংক্রান্ত সকল পরিবহন খরচ kenakatazone সম্পূর্ণ বহন করবে।
 প্রশ্ন মতামত অভিযোগ
ফোন: 01707744499 ইমেইল: info@kenakatazone.com, complain@kenakatazone.com ইনবক্স: https://www.facebook.com/Kenakatazonecom-627558324114668/ook.com/kenakatazone </p>
                                               
                                            @endif
                </div>
                <!-- ./view-product-list-->
      
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>


<!-- Footer -->
@include('pages.footer')

<a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a>
<!-- Script-->
<script type="text/javascript" src="{{ asset('assets/lib/jquery/jquery-1.11.2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery.bxslider/jquery.bxslider.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/owl.carousel/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery.countdown/jquery.countdown.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/lib/jquery-ui/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.actual.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/js/theme-script.js') }}"></script>

</body>
</html>
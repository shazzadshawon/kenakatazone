<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/bootstrap/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/select2/css/select2.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/jquery.bxslider/jquery.bxslider.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/owl.carousel/owl.carousel.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/jquery-ui/jquery-ui.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/animate.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/reset.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/responsive.css') }}" />
    
    <title>Kenakatazone</title>
</head>
<body class="category-page">
<!-- HEADER -->
@include('pages.menu2')
<!-- end header -->
@if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3 style="color: green; text-align: center;"> {{Session::get('message')}}</h3>
</div>
      
@endif
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">
                @if (Session::has('EN'))
                 Contact Us
                @else
                 কন্টাক্ট আস
                @endif
            </span>
        </h2>
        <!-- ../page heading-->
        <div id="contact" class="page-content page-contact">
            <div id="message-box-conact"></div>
            <div class="row">
                <div class="col-sm-6">
                    
                    <div class="contact-form-box">
                         <form action="{{ url('post_contact') }}" method="POST">
                {{ csrf_field() }}
                    <div class="col-sm-12">
                        
                        <div class="contact-form-box">
                            <div class="form-selector">
                                <label>Subject Heading</label>
                                <select class="form-control input-sm" id="subject" name="contact_title">
                                    <option value="Customer service">Customer service</option>
                                    <option value="Webmaster">Webmaster</option>
                                </select>
                            </div>
                            <div class="form-selector">
                                <label>Email address</label>
                                <input type="text" class="form-control input-sm" id="email"  name="contact_email" />
                            </div>
                            <div class="form-selector">
                                <label>Order reference</label>
                                <input type="text" class="form-control input-sm" id="order_reference"  name="contact_reference"/>
                            </div>
                            <div class="form-selector">
                                <label>Message</label>
                                <textarea class="form-control input-sm" rows="10" id="message"  name="contact_description"></textarea>
                            </div>
                            <div class="form-selector">
                                <button id="btn-send-contact" type="submit" class="btn">Send</button>
                            </div>
                        </div>
                    </div>
                </form>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6" id="contact_form_map">
                    <h3 class="page-subheading">Information</h3>
                    <p>KenakataZone is an online shopping &amp; e-commerce site which was established on 1st January 2017 by a group of an entrepreneur.</p>
                    <br/>
                    <!-- <ul>
                        <li>Praesent nec tincidunt turpis.</li>
                        <li>Aliquam et nisi risus.&nbsp;Cras ut varius ante.</li>
                        <li>Ut congue gravida dolor, vitae viverra dolor.</li>
                    </ul> -->
                    <br/>
                    <ul class="store_info">
                        <!--<li><i class="fa fa-home"></i>Art &amp; Creative Interior, H# 265, Free School Street, Kathal Bagan, Dhanmondi, Dhaka-1205.</li>-->
                        <li><i class="fa fa-phone"></i><span>+88 01707744499 </span></li>                
                        <li><i class="fa fa-envelope"></i>Email: <span><a href="mailto:info@kenakatazone.com">info@kenakatazone.com</a>, <a href="mailto:complain@kenakatazone.com">complain@kenakatazone.com</a></span></li>
                    </ul>                
                </div>
            </div>
            
        </div>
    </div>
    <div>
                <!-- Map Section -->
                <div class="map container-fluid no-padding">
                    {{-- <div class="map-canvas" id="map-canvas-contact" data-lat="-37.830846" data-lng="147.611769" data-string="09 Downtown, Store Main St, Victoria, Australia" data-zoom="12"></div> --}}
                    <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3652.0150117502044!2d90.38732251456166!3d23.746844084591114!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sKathalbagan%2C+Free+School+Street%2C+Dhanmondi%2C+Dhaka%2C+Dhaka+Division!5e0!3m2!1sen!2sbd!4v1495349700790" width="1800" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div><!--  Map Section /- -->
                <div class="section-padding"></div>
            </div>
</div>
                    
                


<!-- Footer -->
@include('pages.footer')

<a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a>
<!-- Script-->
<script type="text/javascript" src="{{ asset('assets/lib/jquery/jquery-1.11.2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery.bxslider/jquery.bxslider.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/owl.carousel/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery.countdown/jquery.countdown.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/lib/jquery-ui/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.actual.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/js/theme-script.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>

</body>
</html>
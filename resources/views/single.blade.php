<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/bootstrap/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/select2/css/select2.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/jquery.bxslider/jquery.bxslider.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/owl.carousel/owl.carousel.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/jquery-ui/jquery-ui.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/fancyBox/jquery.fancybox.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/animate.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/reset.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/responsive.css') }}" />
    
    <title>Kenakatazone || Product Details</title>
</head>
<body class="product-page">
<!-- HEADER -->
@include('pages.menu2')
<!-- end header -->
@if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h4 style="text-align: center;"> {{Session::get('message')}}</h4>
</div>
      
@endif
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
      {{--   <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <a href="#" title="Return to Home">Fashion</a>
            <span class="navigation-pipe">&nbsp;</span>
            <a href="#" title="Return to Home">Women</a>
            <span class="navigation-pipe">&nbsp;</span>
            <a href="#" title="Return to Home">Dresses</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Maecenas consequat mauris</span>
        </div> --}}
        <!-- ./breadcrumb -->
        <!-- row -->

        <div class="row">
            
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
                <!-- Product -->
                    <div id="product">
                        <div class="primary-box row">
                            <div class="pb-left-column col-xs-12 col-sm-5">
                                <!-- product-imge-->
                                <div class="product-image">
                                    @php
                                    $productImage = DB::table('product_images')->where('product_id',$productInfo->id)->first();
                                    $imgs = str_replace('product_image\\', '', $productImage->product_image);
                                    @endphp
                                    <div class="product-full">
                                        <img id="product-zoom" src='../{{ $imgs }}' data-zoom-image="../{{ $imgs }}"/>
                                    </div>
                                      
                                      @php
                                        $productImages = DB::table('product_images')->where('product_id',$productInfo->id)->get();
                                      @endphp


                                    <div class="product-img-thumb" id="gallery_01">
                                        <ul class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="21" data-loop="true">
                                        @foreach ($productImages as $productImage)
                                        @php
                                            $imgss = str_replace('product_image\\', '', $productImage->product_image);
                                        @endphp
                                            <li>
                                                <a href="#" data-image="../{{ $imgss }}" data-zoom-image="../{{ $imgss }}">
                                                    <img id="product-zoom"  src="../{{ $imgss }}" /> 
                                                </a>
                                            </li>
                                         @endforeach  
                                        
                                            
                                        </ul>
                                    </div>

                                </div>
                                <!-- product-imge-->
                            </div>
                            <div class="pb-right-column col-xs-12 col-sm-7">
                                <h1 class="product-name">
                                @if (Session::has('EN'))
                    {{ $productInfo->product_name }}
                                @else
                     {{ $productInfo->product_name_bn }}
                                @endif
                                </h1>
                                <div class="product-comments">
                                    <div class="product-star">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </div>
                                    {{-- <div class="comments-advices">
                                        <a href="#">Based  on 3 ratings</a>
                                        <a href="#"><i class="fa fa-pencil"></i> write a review</a>
                                    </div> --}}
                                </div>
                                <div class="product-price-group">
                                        @php
    $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $bn_number = str_replace($replace_array,$search_array,$productInfo->product_price);                         
                                        @endphp
                                       
                                       @if (Session::has('EN'))


                                             @if ($productInfo->discount > 0)
                                                 <div class="content_price">
                                            <span class="price product-price">{{ $productInfo->product_price-($productInfo->product_price*$productInfo->discount)/100 }} TK</span>
                                            <span class="price old-price">{{ $productInfo->product_price }} TK</span>
                                                 </div> 

                                            @else
                                                  <div class="content_price">
                                            <span class="price product-price">{{ $productInfo->product_price }} TK </span>
                                           
                                                 </div>  
                                            @endif
                                        


                                      @else

                                            @if ($productInfo->discount > 0)
                                                 <div class="content_price">
                                            <span class="price product-price">
                                            @php
    $bnDis = $productInfo->product_price-($productInfo->product_price*$productInfo->discount)/100;
    $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $bnDiscount = str_replace($replace_array,$search_array,$bnDis);
                                            @endphp
                                            {{  $bnDiscount}} ট
                                            </span>
                                            <span class="price old-price">{{ $bn_number }} ট</span>
                                                 </div> 

                                            @else
                                                  <div class="content_price">
                                            <span class="price product-price">{{ $bn_number }} ট </span>
                                           
                                                 </div>  
                                            @endif

                                      @endif
                                </div>
                                <div class="info-orther">
                                    <p>Code: {{ $productInfo->product_code }}</p>

                                    <p>Availability: <span class="in-stock">
                                        @if ($productInfo->product_quantity >0)
                                             @if (Session::has('EN'))
                                                 Stock In
                                            @else
                                                 স্টক ইন 
                                            @endif
                                        @else
                                           @if (Session::has('EN'))
                                                 Stock Out
                                            @else
                                                 স্টক আউট 
                                            @endif
                                        @endif
                                    </span></p>
                                    
                                </div>
                                           {!! Form::open(['route' => 'Add-To-Cart.store','files'=>true, 'class'=>'cart']) !!}    
                                              @php
                   
                                       $size = DB::table('product_sizes')
                                       ->where('product_id',$productInfo->id)                                  
                                       ->first();
                                               @endphp
                                  @if(isset($size)!=NULL)
                                <div class="form-option">
                                 
                            
                       
                                    <div class="attributes">
                                        <div class="attribute-label">Size:</div>
                                        <div class="attribute-list">
                                            <select name="size">
                                               @php
                   
                                       $size = DB::table('product_sizes')
                                       ->where('product_id',$productInfo->id)                                  
                                       ->get();
                                               @endphp
                                                
                    @foreach($size as $sizeInfo)
                                                <option value="{{ $sizeInfo->size }}">{{ $sizeInfo->size }}</option>
                    @endforeach
                                            </select>
                                           
                                        </div>
                                        
                                    </div>
                                </div>
                                  @endif
                                <div class="form-action">
                                    <div class="button-group">

                                <input type="hidden" name="product_id" value="{{$productInfo->id}}">
                                <input type="hidden" name="product_name" value="{{$productInfo->product_name}}">
                                <input type="hidden" name="product_name_bn" value="{{$productInfo->product_name_bn}}">
                                <input type="hidden" name="product_code" value="{{$productInfo->product_code}}">
                                <input type="hidden" name="product_price" value="@if($productInfo->discount > 0){{ $productInfo->product_price-($productInfo->product_price*$productInfo->discount)/100}}@else{{$productInfo->product_price}}@endif">
                                <input type="hidden" name="product_quantity" value="1">
                                <input type="hidden" name="publication_status" value="{{$productInfo->publication_status}}">
                                    {{-- <input type="submit" value="ADD TO CART" title=""> --}}
                                   <button class="btn-add-cart" type="submit">
                                       @if (Session::has('EN'))
                                                 Add To Cart
                                            @else
                                                 কার্ট- এ যোগ করুন
                                            @endif
                                   </button>
                              
                                        {{-- <a class="btn-add-cart" href="#">Add to cart</a> --}}
                                    </div>
                                     {!! Form::close() !!}
                                    {{-- <div class="button-group">
                                        <a class="wishlist" href="#"><i class="fa fa-heart-o"></i>
                                        <br>Wishlist</a>
                                       
                                    </div> --}}
                                </div>
                                {{-- <div class="form-share">
                                    <div class="sendtofriend-print">
                                        <a href="javascript:print();"><i class="fa fa-print"></i> Print</a>
                                        <a href="#"><i class="fa fa-envelope-o fa-fw"></i>Send to a friend</a>
                                    </div>
                                    <div class="network-share">
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                        <!-- tab product -->
                        <div class="product-tab">
                            <ul class="nav-tab">
                                <li class="active">
                                    <a aria-expanded="false" data-toggle="tab" href="#product-detail">Product Details</a>
                                </li>
                             
                            </ul>
                            <div class="tab-container">
                                <div id="product-detail" class="tab-panel active">
                                @if (Session::has('EN'))
                    <?php echo $productInfo->description; ?>
                                @else
                     <?php echo $productInfo->description_bn; ?>
                                @endif
                                </div>
                           
                                
                                
                                
                            </div>
                        </div>
                        <!-- ./tab product -->
                        <!-- box product -->
                        
                        <!-- ./box product -->
                        <!-- box product -->
                    
                        <!-- ./box product -->
                    </div>
                <!-- Product -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

<!-- Footer -->
@include('pages.footer')

<a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a>
<!-- Script-->
<script type="text/javascript" src="{{ asset('assets/lib/jquery/jquery-1.11.2.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/lib/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery.bxslider/jquery.bxslider.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/owl.carousel/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery.countdown/jquery.countdown.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery.elevatezoom.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/lib/jquery-ui/jquery-ui.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/lib/fancyBox/jquery.fancybox.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/js/jquery.actual.min.js') }}"></script>


<script type="text/javascript" src="{{ asset('assets/js/theme-script.js') }}"></script>

</body>
</html>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/bootstrap/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/select2/css/select2.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/jquery.bxslider/jquery.bxslider.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/owl.carousel/owl.carousel.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/jquery-ui/jquery-ui.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/animate.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/reset.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/responsive.css') }}" />
    
    <title>Kenakatazone</title>
</head>
<body class="category-page">
<!-- HEADER -->
@include('pages.menu2')
<!-- end header -->

                        
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
       
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
           
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
            
                    <div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
  
        <!-- ./breadcrumb -->
                    @if (Session::has('message'))

                    <div class="alert alert-success" role="alert">
                        <strong></strong><h3> {{Session::get('message')}}</h3>
                    </div>

                    @endif
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
               <!-- page heading-->
                <h2 class="page-heading">
                    <span class="page-heading-title2">
                        @if (Session::has('EN'))
                               Wish List
                                @else
                              ইচ্ছেতালিকা
                                @endif
                                </span>
                 
                </h2>
                <!-- ../page heading-->
          
                <ul class="row list-wishlist">
                 <?php
                            $wishlist = DB::table('wishlists')
                                                ->orderBy('id', 'desc')
                                                ->where('customer_id', Session::get('customer_id'))->get();
                            
                                foreach ($wishlist as $wishlist_info){
                                $product_id=$wishlist_info->product_id;
                                $product= DB::table('products')                                                
                                                ->where('id', $product_id)->first();
                                $product_image= DB::table('product_images')                                                
                                                ->where('product_id', $product_id)->first();
                        ?>
                    <li class="col-sm-3">
                     <div class="button-action">
                          
                            <a href="{{URL::to('/remove-wishlist/'.$wishlist_info->id)}}"><i class="fa fa-close"></i></a>
                        </div>
                        <div class="product-img">
                            <a href="{{URL::to('product-details/'.$wishlist_info->product_id)}}"><img src="../{{$product_image->product_image}}" alt="Product" style="height: 250px;"></a>
                        </div>
                        <h5 class="product-name">
                            <a href="{{URL::to('product-details/'.$wishlist_info->product_id)}}">{{$product->product_name}}</a>
                        </h5>
                     
                     
                       
                    </li>
                 <?php }?>  
        
                </ul>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

<!-- Footer -->
@include('pages.footer')

<a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a>
<!-- Script-->
<script type="text/javascript" src="{{ asset('assets/lib/jquery/jquery-1.11.2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery.bxslider/jquery.bxslider.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/owl.carousel/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery.countdown/jquery.countdown.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/lib/jquery-ui/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.actual.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/js/theme-script.js') }}"></script>

</body>
</html>
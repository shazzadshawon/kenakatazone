<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/bootstrap/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/select2/css/select2.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/jquery.bxslider/jquery.bxslider.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/owl.carousel/owl.carousel.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/jquery-ui/jquery-ui.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/animate.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/reset.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/responsive.css') }}" />
    
    <title>Kenakatazone</title>
</head>
<body class="category-page">
<!-- HEADER -->
@include('pages.menu2')
<!-- end header -->
@if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3 style="color: green; text-align: center;"> {{Session::get('message')}}</h3>
</div>
      
@endif
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Checkout</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">
                                            @if (Session::has('EN'))
                                                Checkout
                                            @else
                                                 চেকআউট 
                                            @endif
            
            </span>
        </h2>
   
 {!! Form::open(['route' => 'Order.store']) !!}
        <div class="page-content checkout-page">
          
            <h3 class="checkout-sep"> Shipping Information</h3>
            <div class="box-border">
                <ul>
                                    
                    <li class="row">
                        
                        <div class="col-sm-6">
                            
                            <label for="first_name_1" class="required">Name</label>
                            <input class="input form-control" type="text" name="name" id="first_name_1">

                        </div><!--/ [col] -->
                        <div class="col-sm-6">
                               <ul class="shipping_method">
                    <li>
                        <p class="subcaption bold"> Shipping Location</p>
                        <label for="radio_button_3"><input type="radio" checked name="location" id="radio_button_3" value="0">InSide Dhaka (Free)</label>
                    </li>

                    <li>                        
                        <label for="radio_button_4"><input type="radio" name="location" id="radio_button_4" value="95"> OutSide Dhaka (95TK)</label>
                    </li>
                </ul>
                        </div>
                      <!--/ [col] -->

                    </li><!--/ .row -->

                    <li class="row">
                        
                        <div class="col-sm-6">
                            
                            <label for="company_name_1">Phone Number</label>
                            <input class="input form-control" type="text" name="phone" id="company_name_1">

                        </div><!--/ [col] -->
                         
                          {{-- <div class="col-sm-6">
                               <ul class="shipping_method">
                    <li>
                        <p class="subcaption bold">Payment Method</p>
                        <label for="radio_button_3"><input type="radio" checked name="payment" id="radio_button_3" value="Bkash">Bkash</label>
                    </li>

                    <li>                        
                        <label for="radio_button_4"><input type="radio" name="payment" id="radio_button_4" value="Cash on delivery"> Cash on delivery</label>
                    </li>
                </ul>
                        </div> --}}
                       <!--/ [col] -->

                    </li><!--/ .row -->

                    <li class="row">

                        <div class="col-xs-6">

                            <label for="address_1" class="required">Address</label>
                
                            <textarea class="input form-control" type="text" name="address" id="address_1"></textarea>

                        </div><!--/ [col] -->

                    </li><!--/ .row -->

                    <!--/ .row -->

                    <!--/ .row -->

                    <!--/ .row -->


                </ul>
                
            </div>
       
    
            <h3 class="checkout-sep">Order Review</h3>
            <div class="box-border">
                <table class="table table-bordered table-responsive cart_summary">
                    <thead>
                        <tr>
                            <th class="cart_product">
                                            @if (Session::has('EN'))
                                                 Product
                                            @else
                                                 প্রোডাক্ট
                                            @endif
                            
                            </th>
                            <th>
                                            @if (Session::has('EN'))
                                                 Description
                                            @else
                                                 ডেসক্রিপশন
                                            @endif
                            
                            </th>
                            <th>
                                            @if (Session::has('EN'))
                                                 Size
                                            @else
                                                 সাইজ
                                            @endif
                            
                            </th>
                            <th>
                                           @if (Session::has('EN'))
                                                 Unit price
                                            @else
                                                ইউনিট প্রাইজ
                                            @endif
                                
                            </th>
                            <th>
                                            @if (Session::has('EN'))
                                                 Qty
                                            @else
                                                কোয়ান্টিটি
                                            @endif
                            
                            </th>
                            <th>
                                            @if (Session::has('EN'))
                                                Total
                                            @else
                                                মোট
                                            @endif
                            </th>
                            <th  class="action"><i class="fa fa-trash-o"></i></th>
                        </tr>
                    </thead>
                    <tbody>

                    @php

                        $i=1;
                        $j=0;
                        $addTocart = DB::table('add_to_carts')->where('session_id', Session::getId())->get(); 
                        
                            @endphp
                        @foreach ($addTocart as $cart)
                           
                           @php
                               $addTocartImage = DB::table('product_images')->where('product_id', $cart->product_id)->first(); 
                           @endphp
                        <tr>
                            <td class="cart_product">
                                <a href="{{ URL::to('/product-details/'.$cart->product_id) }}"><img src="{{$addTocartImage->product_image}}" alt="Product"></a>
                            </td>
                            <td class="cart_description">
                                <p class="product-name"><a href="{{ URL::to('/product-details/'.$cart->product_id) }}">
                                            @if (Session::has('EN'))
                                               {{ $cart->product_name }} 
                                            @else
                                                {{ $cart->product_name_bn }} 
                                            @endif
                                
                                </a></p>
                                <small class="cart_ref">Item Code : #{{ $cart->product_code }}</small><br>
                                {{-- <small><a href="#">Color : Beige</a></small><br>   
                                <small><a href="#">Size : S</a></small> --}}
                            </td>
                            <td class="price"><span> {{ $cart->size }} </span></td>
                            <td class="price"><span>
                                          @php
                       
    $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $BnPrice = str_replace($replace_array,$search_array,$cart->product_price);
                                            @endphp
                                     
                                           @if (Session::has('EN'))
                                                 {{ $cart->product_price }} TK
                                            @else
                                                 {{ $BnPrice }} ট
                                            @endif
                            
                            </span></td>
                            <td class="qty">
                                <h3>
                                            @php
                       
    $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $QTY = str_replace($replace_array,$search_array,$cart->product_quantity);
                                            @endphp
                                     
                                           @if (Session::has('EN'))
                                                 {{ $cart->product_quantity }}
                                            @else
                                                {{ $QTY }} 
                                            @endif
                             
                                </h3>
                            </td>
                            <td class="price">
                                <span>
                                       @php
    $sub_total=$cart->product_price*$cart->product_quantity;
                       
    $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $SubPrice = str_replace($replace_array,$search_array,$sub_total);
                                            @endphp
                                     
                                           @if (Session::has('EN'))
                                                 {{ $sub_total }} TK
                                            @else
                                                 {{ $SubPrice }} ট
                                            @endif
                               

                                </span>
                            </td>
                            <td class="action">
                                <a href="{{URL::to('/remove-cart-product/'.$cart->product_id)}}">Delete item</a>
                            </td>
                        </tr>

                       @php
                                     $j =$j+ $sub_total;
                                     $i++; 
                                @endphp
                                @endforeach

                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2" rowspan="2"></td>
                            <td colspan="3">
                            <b>
                                            @if (Session::has('EN'))
                                              Grand Total
                                            @else
                                                 সর্বমোট

                                            @endif
                             </b>
                             </td>
                            <td colspan="2">
                                    @php
    $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $BnT = str_replace($replace_array,$search_array,$j);
                                    @endphp
                                            @if (Session::has('EN'))
                                                 {{ $j }} TK
                                            @else
                                                 {{ $BnT }} ট
                                            @endif
                            </td>
                        </tr>
                        {{-- <tr>
                            <td colspan="3"><strong>Total</strong></td>
                            <td colspan="2"><strong>122.38 €</strong></td>
                        </tr> --}}
                    </tfoot>    
                </table>
                <button class="button pull-right" type="submit">
                                            @if (Session::has('EN'))
                                                 Place Order
                                            @else
                                                 অর্ডার করুন
                                            @endif                
                </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
</div>

<!-- Footer -->
@include('pages.footer')

<a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a>
<!-- Script-->
<script type="text/javascript" src="{{ asset('assets/lib/jquery/jquery-1.11.2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery.bxslider/jquery.bxslider.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/owl.carousel/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery.countdown/jquery.countdown.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/lib/jquery-ui/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.actual.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/js/theme-script.js') }}"></script>

</body>
</html>
@extends('admin.admin_master')
@section('admin_content')
<div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Members</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
                     @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3> {{Session::get('message')}}</h3>
</div>
      
@endif

        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>SL.</th>
                        <th>Heading</th>  
                         <th>Pazzle</th>  
                         <th>Image</th>
                         <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php 
                    $i=1;
                    $offer = DB::table('pazzles')->get();

                    foreach ($offer as $offerInfo){
                    ?>
                    <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo $offerInfo->heading;?></td>
                         <td>
                             @if ($offerInfo->pazzle==1)
                                One
                             @elseif($offerInfo->pazzle==2)
                               Two
                             @elseif($offerInfo->pazzle==3)
                               Three
                             @elseif($offerInfo->pazzle==4)
                               Four
                             @elseif($offerInfo->pazzle==5)
                               Five
                             @elseif($offerInfo->pazzle==6)
                               Six
                             @elseif($offerInfo->pazzle==7)
                               Right
                             @elseif($offerInfo->pazzle==8)
                               Left
                             @endif
                         </td>
                        <td>
                            <img src="{{ $offerInfo->pazzle_image }}" style="height: 80px;">
                        </td>
                        <td class="center">
                            <?php 
                            if($offerInfo->publication_status==1){
                            ?>
                            <span class="label label-success">Published</span>
                            <?php }else{ ?>
                             <span class="label label-important">Unpublished</span>
                            <?php }?>
                        </td>
                        <td class="center">
                            <?php 
                            if($offerInfo->publication_status==1){
                            ?>
                            <a class="btn btn-success" href="{{URL::to('/unpublished-offer/'.$offerInfo->id)}}">
                                <i class="halflings-icon white thumbs_down"></i>  
                            </a>
                             <?php }else{ ?>
                            <a class="btn btn-danger" href="{{URL::to('/published-offer/'.$offerInfo->id)}}">
                                <i class="halflings-icon thumbs_up"></i>  
                            </a>
                             <?php }?>
                            <a class="btn btn-info" href="{{URL::to('/offer/'.$offerInfo->id.'/edit')}}">
                                <i class="halflings-icon white edit"></i>  
                            </a>
                            <a class="btn btn-danger" href="{{URL::to('/delete-offer/'.$offerInfo->id)}}" onclick="return checkDelete();">
                                <i class="halflings-icon white trash"></i> 
                            </a>
                        </td>
                    </tr>
                    
                    <?php $i++; }?>
                   
                </tbody>
            </table>            
        </div>
    </div>
@endsection
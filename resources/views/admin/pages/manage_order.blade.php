@extends('admin.admin_master')
@section('admin_content')
<div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Members</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
                     @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3> {{Session::get('message')}}</h3>
</div>
      
@endif

        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>SL.</th>
                        <th>OrderNumber</th>
                        <th>CustomerName</th>   
                        <th>Date</th>                         
                         <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php 
                    $i=1;
                    foreach ($orders as $order_info){
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                         <td>#<?php echo $order_info->order_number; ?></td>
                         
                            <td class="center"><?php echo $order_info->customer_name; ?></td>
                            <td class="center"><?php echo date('M j, Y h:ia', strtotime($order_info->created_at)) ?></td>
                            <td class="center">
                            <?php 
                            if($order_info->publication_status==1){
                            ?>
                            <span class="label label-success">Delivered</span>
                            <?php }else if($order_info->publication_status==0){ ?>
                             <span class="label label-important">Pending</span>
                            <?php }else if($order_info->publication_status==2){ ?>
                             <span class="label label-Warning">Refuse Oder</span>
                            <?php }?>
                        </td>
                        <td class="center">                     
                            
                            <a class="btn btn-success" href="{{URL::to('/view-order/'.$order_info->order_number)}}">
                                <i class="halflings-icon white zoom-in"></i> 
                            </a>
                            <a class="btn btn-danger" href="{{URL::to('/delete-order/'.$order_info->order_number)}}" onclick="return checkDelete();">
                                <i class="halflings-icon white trash"></i> 
                            </a>
                        </td>
                    </tr>
                    
                    <?php $i++;}?>
                   
                </tbody>
            </table>            
        </div>
    </div>
@endsection
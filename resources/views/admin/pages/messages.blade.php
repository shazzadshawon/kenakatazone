@extends('admin.admin_master')
@section('admin_content')
<div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Messages</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
                     @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3> {{Session::get('message')}}</h3>
</div>
      
@endif

        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>SL.</th>
                        <th>Title</th>
                        <th>Email</th>   
                        <th>reference</th>   
                         <th>Description</th> 
                        
                        {{-- <th>Actions</th> --}}
                    </tr>
                </thead>   
                <tbody>
                    <?php 
                    $i=1;
                    foreach ($messages as $product_info){
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                            
                            <td class="center"><?php echo $product_info->contact_title; ?></td> 
                            <td class="center"><?php echo $product_info->contact_email; ?></td>
                            <td class="center"><?php echo $product_info->contact_reference; ?></td>
                            <td class="center"><?php echo $product_info->contact_description; ?></td>
                           
                       
                    </tr>
                    
                    <?php $i++;}?>
                   
                </tbody>
            </table>            
        </div>
    </div>
@endsection
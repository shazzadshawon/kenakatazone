<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/bootstrap/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/select2/css/select2.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/jquery.bxslider/jquery.bxslider.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/owl.carousel/owl.carousel.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/jquery-ui/jquery-ui.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/animate.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/reset.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/responsive.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/option4.css') }}" />
    <title>Kenakatazone</title>
    <link rel="icon" type="image/x-icon" href="../images/logo.jpg" />
</head>

<body class="home option4">
<!-- HEADER -->
<div id="header" class="header">
    
    @include('pages.top_header')
    <!--/.top-header -->
    @include('pages.header')
    <!-- MAIN HEADER -->
    @include('pages.menu')
<!-- end header -->
<!-- Home slideder-->
<div id="home-slider">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 slider-left"></div>
            <div class="col-sm-9 header-top-right">
                <div class="header-top-right-wapper">
                    <div class="homeslider">
                        <div class="content-slide">
                            <ul id="contenhomeslider">
                            @php
                          $slider = DB::table('slider_images')->where('publication_status',1)->get();
                       $j = 1;
                      @endphp
                      @foreach ($slider as $sliderInfo)
                              <li><img alt="Funky roots" src="../{{ $sliderInfo->slider_image }}" title="Funky roots" /></li>
                    @endforeach
                             {{--  <li><img alt="Funky roots" src="assets/data/option4/slider2.jpg" title="Funky roots" /></li>
                              <li><img  alt="Funky roots" src="assets/data/option4/slider3.jpg" title="Funky roots" /></li> --}}

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3 style="text-align: center;"> {{Session::get('message')}}</h3>
</div>
      
@endif
<!-- END Home slideder-->

        <!---  Hot Deal ---------------------->

          @include('partials.hot_deal')


        <!---  New Arrivals ---------------------->

        @include('partials.new_arrival')

        <!---  special offer ---------------------->

        @include('partials.special_offer')

        <!---  Active menu ---------------------->

        @include('partials.active_category')
        
        <!---  Brand Logo Slider ---------------------->

        @include('partials.logo_brand')
            
        
<!-- Footer -->
       @include('pages.footer')

<a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a>
<!-- Script-->
<script type="text/javascript" src="assets/lib/jquery/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="assets/lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/lib/select2/js/select2.min.js"></script>
<script type="text/javascript" src="assets/lib/jquery.bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="assets/lib/owl.carousel/owl.carousel.min.js"></script>
<!-- COUNTDOWN -->
<script type="text/javascript" src="assets/lib/countdown/jquery.plugin.js"></script>
<script type="text/javascript" src="assets/lib/countdown/jquery.countdown.js"></script>
<!-- ./COUNTDOWN -->
<script type="text/javascript" src="assets/js/jquery.actual.min.js"></script>
<script type="text/javascript" src="assets/js/theme-script.js"></script>
<script type="text/javascript" src="assets/js/option4.js"></script>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/bootstrap/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/select2/css/select2.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/jquery.bxslider/jquery.bxslider.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/owl.carousel/owl.carousel.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/jquery-ui/jquery-ui.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/animate.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/reset.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/responsive.css') }}" />
    
    <title>Kenakatazone</title>
</head>
<body class="category-page">
<!-- HEADER -->
@include('pages.menu2')
<!-- end header -->
@if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3 style="color: green; text-align: center;"> {{Session::get('message')}}</h3>
</div>
      
@endif
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
       
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
           
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
               
                     <div class="page-content">
            <div class="row">
                <div class="col-sm-6">
                      {!! Form::open(['route' => 'customer.store']) !!}
                    <div class="box-authentication">
                        <h3>Create an account</h3>
                        <p>Please enter your email address to create an account.</p>
                        <label for="emmail_register">Name</label>
                        <input id="emmail_register" type="text" class="form-control" name="customer_name">
                        <label for="emmail_register">Phone Number</label>
                        <input id="emmail_register" type="text" class="form-control" name="phone_number">
                        <label for="emmail_register">Address</label>                        
                        <textarea id="emmail_register" name="address" class="form-control"></textarea>
                        <label for="emmail_register">Email address</label>
                        <input id="emmail_register" type="email" name="email_adderss" class="form-control">
                        <label for="emmail_register">Password</label>
                        <input id="emmail_register" type="password" name="password" class="form-control">
                    
                        <button class="button"><i class="fa fa-user"></i> SingUp</button>
                    </div>
                   {!! Form::close() !!}
                </div>
                <div class="col-sm-6">
                    <div class="box-authentication">
                        <h3>Already registered?</h3>
                        {!! Form::open(['url' => '/customer-login-check', 'method'=>'POST']) !!}
                        <label for="emmail_login">Email address</label>
                        <input id="emmail_login" type="text" class="form-control" name="email_adderss">
                        <label for="password_login">Password</label>
                        <input id="password_login" type="password" class="form-control" name="password">
                        {{-- <p class="forgot-pass"><a href="#">Forgot your password?</a></p> --}}
                        <button class="button"><i class="fa fa-lock"></i> Login</button>
                      {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
                </div>
                <!-- ./view-product-list-->
      
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

<!-- Footer -->
@include('pages.footer')

<a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a>
<!-- Script-->
<script type="text/javascript" src="{{ asset('assets/lib/jquery/jquery-1.11.2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery.bxslider/jquery.bxslider.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/owl.carousel/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery.countdown/jquery.countdown.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/lib/jquery-ui/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.actual.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/js/theme-script.js') }}"></script>

</body>
</html>
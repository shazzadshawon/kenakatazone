-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 19, 2017 at 11:19 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kenakatazonef`
--

-- --------------------------------------------------------

--
-- Table structure for table `add_to_carts`
--

CREATE TABLE `add_to_carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_name_bn` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_code` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_price` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_quantity` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(10) UNSIGNED NOT NULL,
  `admin_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email_address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `admin_name`, `email_address`, `password`) VALUES
(1, 'Mahmud Hira', 'mahmudhiracse@gmail.com', '69eee85465438d9c013b14bb7210cc7d');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_name_bn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `category_name_bn`, `publication_status`, `created_at`, `updated_at`) VALUES
(33, 'MEN’S FASHION', 'মেন ফ্যাশন', 1, '2017-06-07 03:00:56', '2017-06-07 03:07:27'),
(34, 'WOMEN’S FASHION', 'ওমেন্স ফ্যাশন', 1, '2017-06-07 03:15:25', '2017-06-07 03:15:25'),
(35, 'MOBILES & TABLETS', 'মোবাইল ও ট্যাবলেট', 1, '2017-06-07 03:20:19', '2017-06-07 03:20:19'),
(36, 'COMPUTER PRODUCTS', 'কম্পিউটার প্রোডাক্ট', 1, '2017-06-07 03:22:03', '2017-06-07 03:22:03'),
(37, 'ELECTRONICS PRODUCTS', 'ইলেক্ট্রনিক্স প্রোডাক্ট', 1, '2017-06-07 03:23:21', '2017-06-07 03:23:21'),
(38, 'HOME & LIVING', 'হোম & লিভিং', 1, '2017-06-07 03:27:25', '2017-06-07 03:27:25');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email_adderss` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `customer_name`, `phone_number`, `address`, `email_adderss`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Mahmud Hira', '01675646860', 'Dhaka', 'mahmud@gmail.com', '69eee85465438d9c013b14bb7210cc7d', '2017-01-09 03:50:23', '2017-01-09 03:50:23'),
(2, 'Nilim', '01675646860', 'Narayangamj', 'nilim@gmail.com', '69eee85465438d9c013b14bb7210cc7d', '2017-01-09 03:51:48', '2017-01-09 03:51:48'),
(3, 'Sabbir', '01675646860', 'Dhaka', 'sabbir@gmail.com', '69eee85465438d9c013b14bb7210cc7d', '2017-01-09 03:54:18', '2017-01-09 03:54:18'),
(4, 'ripon', '15466288', 'Dhaka', 'ripon@gamil.com', '69eee85465438d9c013b14bb7210cc7d', '2017-01-09 03:55:38', '2017-01-09 03:55:38'),
(5, 'Mahmud Hira', '01675646860', 'Dhaka', 'mahmud@gmail.com', '69eee85465438d9c013b14bb7210cc7d', '2017-06-19 00:59:33', '2017-06-19 00:59:33');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_01_01_124411_create_admins_table', 1),
(4, '2017_01_01_135930_create_categories_table', 2),
(5, '2017_01_02_085340_create_sub_categories_table', 3),
(6, '2017_01_02_133054_create_slider_images_table', 4),
(7, '2017_01_03_094319_create_product_images_table', 5),
(8, '2017_01_03_100037_create_products_table', 5),
(9, '2017_01_09_072750_create_customers_table', 6),
(10, '2017_01_09_123439_create_wishlists_table', 7),
(11, '2017_01_10_133258_create_add_to_carts_table', 8),
(12, '2017_01_11_114149_create_orders_table', 9),
(13, '2017_01_11_114233_create_shipping_addresses_table', 9),
(14, '2017_05_04_081139_create_sub_sub_categories_table', 10),
(15, '2017_05_08_080323_create_pazzles_table', 11),
(16, '2017_05_15_103110_create_subscribes_table', 12),
(17, '2017_06_18_063013_create_product_sizes_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_code` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_price` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_quantity` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `order_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `product_id`, `customer_id`, `product_name`, `product_code`, `product_price`, `product_quantity`, `size`, `order_number`, `session_id`, `publication_status`, `created_at`, `updated_at`) VALUES
(1, 78, 1, 'Shirt', 'shg664543', '520', '1', 'None', '26432', '4a2GK7yVoWQXm3CeYzKcc57d8PVlZIjaqYz3Hnq7', 0, '2017-06-19 02:32:55', '2017-06-19 02:32:55'),
(2, 72, 1, 'Polo T-shirt', 'PT654156', '640', '1', 'L', '26432', '4a2GK7yVoWQXm3CeYzKcc57d8PVlZIjaqYz3Hnq7', 0, '2017-06-19 02:32:55', '2017-06-19 02:32:55'),
(3, 70, 1, 'T-shirt', 'TS-6546', '427.5', '2', 'None', '9849', 'b0CyxOH2JxwwMPMHC2PLSyElkC70AaNDgIi7sTVK', 0, '2017-06-19 02:40:18', '2017-06-19 02:40:18'),
(4, 73, 1, 'Polo', 'PL515646', '141', '1', 'None', '9849', 'b0CyxOH2JxwwMPMHC2PLSyElkC70AaNDgIi7sTVK', 0, '2017-06-19 02:40:18', '2017-06-19 02:40:18'),
(5, 72, 1, 'Polo T-shirt', 'PT654156', '640', '1', 'M', '9849', 'b0CyxOH2JxwwMPMHC2PLSyElkC70AaNDgIi7sTVK', 0, '2017-06-19 02:40:18', '2017-06-19 02:40:18'),
(6, 71, 1, 'T-Shirt', 'SF54145198', '600', '1', 'None', '28606', 'oTAj5QwOOc3uxNnPtPqNF92zAnJF5Mx1Z05mnu1k', 0, '2017-06-19 02:43:53', '2017-06-19 02:43:53');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pazzles`
--

CREATE TABLE `pazzles` (
  `id` int(10) UNSIGNED NOT NULL,
  `heading` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pazzle` tinyint(4) NOT NULL,
  `pazzle_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pazzles`
--

INSERT INTO `pazzles` (`id`, `heading`, `pazzle`, `pazzle_image`, `publication_status`, `created_at`, `updated_at`) VALUES
(9, 'Hot offer', 1, 'pazzle_image\\images (3).jpg', 1, '2017-06-11 02:26:56', '2017-06-11 02:26:56'),
(10, 'Hot offer', 1, 'pazzle_image\\images (4).jpg', 1, '2017-06-11 02:26:56', '2017-06-11 02:26:56'),
(11, 'Hot offer', 1, 'pazzle_image\\stock-vector-hot-sale-banner-this-weekend-special-offer-big-sale-discount-up-to-off-vector-illustration-493529716.jpg', 1, '2017-06-11 02:26:56', '2017-06-11 02:26:56'),
(12, 'New Arriaval', 2, 'pazzle_image\\dsdssssss.jpg', 1, '2017-06-11 03:01:51', '2017-06-11 03:01:51'),
(13, 'New Arriaval', 2, 'pazzle_image\\dvdvds.jpg', 1, '2017-06-11 03:01:51', '2017-06-11 03:01:51'),
(14, 'New Arriaval', 2, 'pazzle_image\\sdvsdfgsdf.jpg', 1, '2017-06-11 03:01:51', '2017-06-11 03:01:51'),
(15, 'Special Offer', 3, 'pazzle_image\\GENTS-JEANS-PENT.jpg', 1, '2017-06-11 22:59:43', '2017-06-11 22:59:43'),
(16, 'Special Offer', 3, 'pazzle_image\\images (1).jpg', 1, '2017-06-11 22:59:43', '2017-06-11 22:59:43'),
(17, 'Special Offer', 3, 'pazzle_image\\imagessdf sdf.jpg', 1, '2017-06-11 22:59:44', '2017-06-11 22:59:44'),
(18, 'Men Fashion', 33, 'pazzle_image\\d fdh dfh df.jpg', 1, '2017-06-12 23:46:34', '2017-06-12 23:46:34'),
(19, 'Men Fashion', 33, 'pazzle_image\\GENTS-JEANS-PENT.jpg', 1, '2017-06-12 23:46:34', '2017-06-12 23:46:34'),
(20, 'Men Fashion', 33, 'pazzle_image\\imagessdf sdf.jpg', 1, '2017-06-12 23:46:34', '2017-06-12 23:46:34'),
(21, 'Women ', 34, 'pazzle_image\\6ce4c4661831f14ebb0c4a6e90a2c854.jpg', 1, '2017-06-13 00:22:42', '2017-06-13 00:22:42'),
(22, 'Women ', 34, 'pazzle_image\\47529d8012dcf399c1e9d1f13aad50d2.jpg', 1, '2017-06-13 00:22:42', '2017-06-13 00:22:42'),
(23, 'Women ', 34, 'pazzle_image\\a19c75c423bf6f7eafa276eb8a41435a.jpg', 1, '2017-06-13 00:22:42', '2017-06-13 00:22:42'),
(24, 'Mobile', 35, 'pazzle_image\\lava-a67-dual-sim-android-mobile-phone-medium_3a86d70832ad27694f49cea1aba8dd81.jpg', 1, '2017-06-13 00:24:23', '2017-06-13 00:24:23'),
(25, 'Mobile', 35, 'pazzle_image\\redmi-note-4g-vang_1472636761.jpg', 1, '2017-06-13 00:24:23', '2017-06-13 00:24:23'),
(26, 'Mobile', 35, 'pazzle_image\\xiaomi-redmi-note4-sn-1.jpg', 1, '2017-06-13 00:24:23', '2017-06-13 00:24:23'),
(27, 'Computer', 36, 'pazzle_image\\download (3).jpg', 1, '2017-06-13 00:25:44', '2017-06-13 00:25:44'),
(28, 'Computer', 36, 'pazzle_image\\download (4).jpg', 1, '2017-06-13 00:25:44', '2017-06-13 00:25:44'),
(29, 'Logo', 4, 'pazzle_image\\dsfsdf.jpg', 1, '2017-06-13 22:32:12', '2017-06-13 22:32:12'),
(30, 'Logo', 4, 'pazzle_image\\dssdfsd.jpg', 1, '2017-06-13 22:32:12', '2017-06-13 22:32:12'),
(31, 'Logo', 4, 'pazzle_image\\fgsdfg.jpg', 1, '2017-06-13 22:32:12', '2017-06-13 22:32:12'),
(32, 'Logo', 4, 'pazzle_image\\hdbfds.jpg', 1, '2017-06-13 22:32:12', '2017-06-13 22:32:12'),
(33, 'Logo', 4, 'pazzle_image\\Samsung-Housing-SHD-3000F4-2066-p.png', 1, '2017-06-13 22:32:12', '2017-06-13 22:32:12'),
(34, 'Logo', 4, 'pazzle_image\\sdafsdfsdf.png', 1, '2017-06-13 22:32:12', '2017-06-13 22:32:12');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `sub_sub_category_id` int(11) NOT NULL,
  `product_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_name_bn` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_code` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_price` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_quantity` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `discount` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `description_bn` longtext COLLATE utf8_unicode_ci NOT NULL,
  `offer_status` tinyint(4) NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `sub_category_id`, `sub_sub_category_id`, `product_name`, `product_name_bn`, `product_code`, `product_price`, `product_quantity`, `discount`, `description`, `description_bn`, `offer_status`, `publication_status`, `created_at`, `updated_at`) VALUES
(70, 33, 34, 9, 'T-shirt', 'টি- শার্ট', 'TS-6546', '450', '50', '5', '&nbsp;egfyuesgh syagdf sdfba dasdbfubsduab fusdaufbgsauydfys&nbsp;', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">টি- শার্ট এক্সপোর্ট কোয়ালিটি&nbsp;টি- শার্ট এক্সপোর্ট কোয়ালিটিটি- শার্ট এক্সপোর্ট কোয়ালিটি</span></font>', 1, 1, '2017-06-08 02:56:36', '2017-06-08 02:56:36'),
(71, 33, 34, 9, 'T-Shirt', 'টি- শার্ট ', 'SF54145198', '600', '20', '', 'jdhs ushdb fusdbhf bsdbhf bshdbfhbasdhb fsdfhbdsbfhb sbdfbdsuf', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">টি- শার্ট এক্সপোর্ট কোয়ালিটি&nbsp;টি- শার্ট এক্সপোর্ট কোয়ালিটি&nbsp;টি- শার্ট এক্সপোর্ট কোয়ালিটি</span></font>', 1, 1, '2017-06-08 03:16:54', '2017-06-08 03:16:54'),
(72, 33, 34, 10, 'Polo T-shirt', 'পোলো টি শার্ট ', 'PT654156', '800', '20', '20', 'dsfh bhsbdf sdabn fbsdh bhsdhf bsdhf dfhdsjf ds b jjj', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">পোলো টি শার্ট &nbsp;&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;</span></font>', 1, 1, '2017-06-08 03:23:41', '2017-06-08 03:23:41'),
(73, 33, 34, 10, 'Polo', 'পোলো টি শার্ট ', 'PL515646', '150', '50', '6', 'hsdf bhsdbh bbshdh gsbd sdifn<br>dskj fnsdj sdnf adsifnisdnfi', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;</span></font>', 1, 1, '2017-06-08 03:25:50', '2017-06-08 03:25:50'),
(74, 33, 34, 13, 'Pant', 'প্যান্ট', 'PT65465', '2000', '20', '3', 'fdh shg uidhfsig hdsfg dsfgdf<br>sdjf sif &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;drgjifdj gdfgdf', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">প্যান্ট এক্সপোর্ট কোয়ালিটিপ্যান্ট এক্সপোর্ট কোয়ালিটিপ্যান্ট এক্সপোর্ট কোয়ালিটিপ্যান্ট এক্সপোর্ট কোয়ালিটিপ্যান্ট এক্সপোর্ট কোয়ালিটিপ্যান্ট এক্সপোর্ট কোয়ালিটিপ্যান্ট এক্সপোর্ট কোয়ালিটিপ্যান্ট এক্সপোর্ট কোয়ালিটিপ্যান্ট এক্সপোর্ট কোয়ালিটিপ্যান্ট এক্সপোর্ট কোয়ালিটি</span></font>', 1, 1, '2017-06-08 03:52:06', '2017-06-08 03:52:06'),
(75, 33, 34, 11, 'Shirt', 'শার্ট', 'sh6546541', '1550', '30', '', 'hjhfd hbdsfh hdsbfuhvbs hbdhsbsd<br>SD fjdsfi fipusd jsdof d\'s<br>dkjs fnsdufnguosd', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">&nbsp;&nbsp;</span></font><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">শার্টশার্টশার্টশার্টশার্টশার্টশার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্টশার্টশার্টশার্ট</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">শার্টশার্টশার্টশার্টশার্টশার্টশার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্টশার্টশার্টশার্ট &nbsp;</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">শার্টশার্টশার্টশার্টশার্টশার্টশার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্টশার্টশার্টশার্ট</span>', 1, 1, '2017-06-10 21:44:06', '2017-06-10 21:44:06'),
(76, 33, 34, 14, 'Jeans', 'জিন্স ', 'JE6549', '1500', '20', '', 'hsbdf hsdb sdb fbsdflbsdfi adhb jcnv bjhdcbugdfgbsdf lsdfi sdfsdf sal bfsd\\as\\]as<div>asd</div><div>as</div><div>dasdf</div>', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">জিন্স &nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্</span></font><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;</span></font></div>', 2, 1, '2017-06-11 02:43:27', '2017-06-11 02:43:27'),
(77, 33, 34, 9, 'T-shirt', 'টি -শার্ট  ', 'TS65165', '600', '60', '6', 'kjdsbn j sdhf ds dsg dg sd sd sdf sd fsd fs]dsdfds\\sd sd fsd<div>&nbsp;sdf sdfgds</div>', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;</span></font><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">টি -শার্ট &nbsp;টি -শার্ট &nbsp;</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;</span></font></div>', 2, 1, '2017-06-11 02:59:06', '2017-06-11 02:59:06'),
(78, 33, 34, 11, 'Shirt', 'শার্ট ', 'shg664543', '650', '50', '20', 'dshf sdhbsd blsdbfvh sdbhbvhds bhbsdh vshdbv dsbfv dsbhfvb hsdbvfh sbdhfv dsb', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;</span></font><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\"><br></span></font></div>', 3, 1, '2017-06-11 03:17:36', '2017-06-11 03:17:36'),
(79, 33, 34, 10, 'Polo\'s', 'পোলো ', 'PL56dsfg', '1500', '20', '6', 'polo kajsdfn sbd hdsfg sadg dhghdfg', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">পোলো&nbsp;পোলো&nbsp;পোলো&nbsp;পোলো&nbsp;পোলো&nbsp;পোলো&nbsp;পোলো&nbsp;পোলো&nbsp;পোলো&nbsp;পোলো&nbsp;</span><br><span style=\"font-size: 13.3333px;\">পোলো&nbsp;পোলো&nbsp;পোলো&nbsp;পোলো&nbsp;</span></font>', 3, 1, '2017-06-12 00:07:47', '2017-06-12 00:07:47'),
(80, 33, 35, 16, 'Panjabis & Sherwanis', 'পাঞ্জাবি ও শেরওয়ানি', 'PS564196', '5000', '20', '', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Panjabis &amp; Sherwanis</span></font>', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">&nbsp;</span></font><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">পাঞ্জাবি ও শেরওয়ানি&nbsp;</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">পাঞ্জাবি ও শেরওয়ানি&nbsp;</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">পাঞ্জাবি ও শেরওয়ানি&nbsp;</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">পাঞ্জাবি ও শেরওয়ানি&nbsp;</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">পাঞ্জাবি ও শেরওয়ানি&nbsp;</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">পাঞ্জাবি ও শেরওয়ানি&nbsp;</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">পাঞ্জাবি ও শেরওয়ানি&nbsp;</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">পাঞ্জাবি ও শেরওয়ানি&nbsp;</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">পাঞ্জাবি ও শেরওয়ানি&nbsp;</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">পাঞ্জাবি ও শেরওয়ানি&nbsp;</span>', 3, 1, '2017-06-13 00:04:52', '2017-06-13 00:04:52');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `product_image_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`product_image_id`, `product_id`, `product_image`, `created_at`, `updated_at`) VALUES
(8, 4, 'product_image\\f1.jpg', '2017-01-03 21:50:08', '2017-01-03 21:50:08'),
(9, 4, 'product_image\\f2.jpg', '2017-01-03 21:50:08', '2017-01-03 21:50:08'),
(10, 20, 'product_image\\bkash.jpg', '2017-01-04 06:38:09', '2017-01-04 06:38:09'),
(11, 20, 'product_image\\cad1.jpg', '2017-01-04 06:38:09', '2017-01-04 06:38:09'),
(12, 10, 'product_image\\cad1.jpg', '2017-01-04 07:02:19', '2017-01-04 07:02:19'),
(14, 46, 'product_image\\asdas.jpg', '2017-01-04 21:46:33', '2017-01-04 21:46:33'),
(15, 46, 'product_image\\asdasd.jpg', '2017-01-04 21:46:33', '2017-01-04 21:46:33'),
(16, 47, 'product_image\\asdas.jpg', '2017-01-04 21:51:10', '2017-01-04 21:51:10'),
(17, 47, 'product_image\\cad2.jpg', '2017-01-04 21:51:10', '2017-01-04 21:51:10'),
(19, 10, 'product_image\\f2.jpg', '2017-01-05 01:25:43', '2017-01-05 01:25:43'),
(20, 48, 'product_image\\b1-1.jpg', '2017-01-05 06:25:35', '2017-01-05 06:25:35'),
(21, 48, 'product_image\\b2.jpg', '2017-01-05 06:25:35', '2017-01-05 06:25:35'),
(22, 49, 'product_image\\739d383c78a4b2d66dc8414f0f2f9976.jpg', '2017-01-08 07:46:33', '2017-01-08 07:46:33'),
(23, 49, 'product_image\\1681160-transcends-original-imae6r6quszhbkgp.jpeg', '2017-01-08 07:46:33', '2017-01-08 07:46:33'),
(24, 49, 'product_image\\Archies-Peacock-Green-Ceramic-Showpiece-7005-721883-1-pdp_slider_m.jpg', '2017-01-08 07:46:33', '2017-01-08 07:46:33'),
(25, 49, 'product_image\\Paras-Green-Peacock-Showpiece-SDL356069069-1-2b687.jpg', '2017-01-08 07:46:33', '2017-01-08 07:46:33'),
(26, 50, 'product_image\\images.jpg', '2017-01-08 07:51:29', '2017-01-08 07:51:29'),
(27, 50, 'product_image\\Paras-Royal-Romantic-Couple-Showpiece-SDL894211066-1-d2670.jpg', '2017-01-08 07:51:29', '2017-01-08 07:51:29'),
(28, 51, 'product_image\\images (2).jpg', '2017-01-08 07:54:17', '2017-01-08 07:54:17'),
(29, 51, 'product_image\\NVR-Multicolour-Water-Fountain-Showpiece-SDL190075992-1-44bad.jpg', '2017-01-08 07:54:17', '2017-01-08 07:54:17'),
(30, 52, 'product_image\\images (3).jpg', '2017-01-08 07:55:33', '2017-01-08 07:55:33'),
(31, 52, 'product_image\\images (4).jpg', '2017-01-08 07:55:33', '2017-01-08 07:55:33'),
(32, 52, 'product_image\\sa020-sancheti-art-400x400-imaefa8tgazdcykj.jpeg', '2017-01-08 07:55:33', '2017-01-08 07:55:33'),
(33, 53, 'product_image\\hm2.jpg', '2017-01-10 00:14:33', '2017-01-10 00:14:33'),
(34, 53, 'product_image\\hm3.jpg', '2017-01-10 00:14:33', '2017-01-10 00:14:33'),
(35, 54, 'product_image\\hm4.jpg', '2017-01-10 00:16:55', '2017-01-10 00:16:55'),
(36, 54, 'product_image\\hm5.jpg', '2017-01-10 00:16:55', '2017-01-10 00:16:55'),
(37, 55, 'product_image\\cl1.jpg', '2017-01-10 00:18:32', '2017-01-10 00:18:32'),
(38, 55, 'product_image\\cl2.jpg', '2017-01-10 00:18:32', '2017-01-10 00:18:32'),
(39, 56, 'product_image\\cl3.jpg', '2017-01-10 00:20:33', '2017-01-10 00:20:33'),
(40, 56, 'product_image\\cl4.jpg', '2017-01-10 00:20:33', '2017-01-10 00:20:33'),
(41, 57, 'product_image\\j1.jpg', '2017-01-10 00:22:27', '2017-01-10 00:22:27'),
(42, 57, 'product_image\\j2.jpg', '2017-01-10 00:22:27', '2017-01-10 00:22:27'),
(43, 58, 'product_image\\17015346_661932453986413_435113642_o.jpg', '2017-05-08 06:42:35', '2017-05-08 06:42:35'),
(44, 58, 'product_image\\cb-29_1.jpg', '2017-05-08 06:42:35', '2017-05-08 06:42:35'),
(45, 59, 'product_image\\15697758_1192167880820956_556540586274312860_n.jpg', '2017-05-08 23:15:29', '2017-05-08 23:15:29'),
(46, 60, 'product_image\\299.jpg', '2017-05-08 23:17:02', '2017-05-08 23:17:02'),
(47, 61, 'product_image\\15781207_1192167537487657_6975721031401627982_n.jpg', '2017-05-08 23:18:40', '2017-05-08 23:18:40'),
(48, 62, 'product_image\\1-250x250.jpg', '2017-05-08 23:20:46', '2017-05-08 23:20:46'),
(49, 62, 'product_image\\15697365_1192167547487656_6482461259784418848_n.jpg', '2017-05-08 23:20:46', '2017-05-08 23:20:46'),
(50, 63, 'product_image\\Hospital Furniture.jpg', '2017-05-15 00:56:41', '2017-05-15 00:56:41'),
(51, 64, 'product_image\\Home Furniture.jpg', '2017-05-15 00:58:38', '2017-05-15 00:58:38'),
(52, 65, 'product_image\\Interior.jpg', '2017-05-15 01:03:00', '2017-05-15 01:03:00'),
(53, 66, 'product_image\\Up Coming Products..jpg', '2017-05-15 02:15:14', '2017-05-15 02:15:14'),
(54, 67, 'product_image\\SAVE UP TO 50% OFF. COST EFFECTIVITY.jpg', '2017-05-15 02:16:14', '2017-05-15 02:16:14'),
(55, 68, 'product_image\\INDUSTRIAL FURNITURE.jpg', '2017-05-15 02:18:31', '2017-05-15 02:18:31'),
(56, 69, 'product_image\\Victorian breakfront display cabinet DP35.JPG', '2017-05-15 02:19:27', '2017-05-15 02:19:27'),
(57, 70, 'product_image\\cnhgfhjghj.jpg', '2017-06-08 02:56:36', '2017-06-08 02:56:36'),
(58, 70, 'product_image\\fgfgdfgf.jpg', '2017-06-08 02:56:36', '2017-06-08 02:56:36'),
(59, 71, 'product_image\\fdsfsdfsd.jpeg', '2017-06-08 03:16:54', '2017-06-08 03:16:54'),
(60, 71, 'product_image\\fgfgdfgf.jpg', '2017-06-08 03:16:54', '2017-06-08 03:16:54'),
(61, 71, 'product_image\\gdsgdfgdfsgdfg.jpg', '2017-06-08 03:16:54', '2017-06-08 03:16:54'),
(62, 72, 'product_image\\asdasdasd.jpg', '2017-06-08 03:23:41', '2017-06-08 03:23:41'),
(63, 72, 'product_image\\dfsgdfgdsfgrrr.jpg', '2017-06-08 03:23:41', '2017-06-08 03:23:41'),
(65, 73, 'product_image\\gdsgdfgdfsgdfg.jpg', '2017-06-08 03:25:50', '2017-06-08 03:25:50'),
(66, 73, 'product_image\\images (1).jpg', '2017-06-08 03:25:50', '2017-06-08 03:25:50'),
(67, 74, 'product_image\\GENTS-JEANS-PENT.jpg', '2017-06-08 03:52:06', '2017-06-08 03:52:06'),
(68, 74, 'product_image\\images (2).jpg', '2017-06-08 03:52:06', '2017-06-08 03:52:06'),
(69, 74, 'product_image\\sadfasdasd.jpg', '2017-06-08 03:52:06', '2017-06-08 03:52:06'),
(70, 75, 'product_image\\d fdh dfh df.jpg', '2017-06-10 21:44:06', '2017-06-10 21:44:06'),
(71, 75, 'product_image\\imagessdf sdf.jpg', '2017-06-10 21:44:06', '2017-06-10 21:44:06'),
(72, 76, 'product_image\\asdasdas.jpg', '2017-06-11 02:43:27', '2017-06-11 02:43:27'),
(73, 76, 'product_image\\images (2).jpg', '2017-06-11 02:43:27', '2017-06-11 02:43:27'),
(74, 77, 'product_image\\downloadsadasdas.jpg', '2017-06-11 02:59:06', '2017-06-11 02:59:06'),
(75, 77, 'product_image\\drjhghj.jpg', '2017-06-11 02:59:06', '2017-06-11 02:59:06'),
(76, 78, 'product_image\\11460104200751-Highlander-Blue-Slim-Fit-Denim-Shirt-7421460104200117-1.jpg', '2017-06-11 03:17:36', '2017-06-11 03:17:36'),
(77, 78, 'product_image\\d fdh dfh df.jpg', '2017-06-11 03:17:36', '2017-06-11 03:17:36'),
(78, 79, 'product_image\\asdasdasd.jpg', '2017-06-12 00:07:48', '2017-06-12 00:07:48'),
(79, 79, 'product_image\\dfsgdfgdsfgrrr.jpg', '2017-06-12 00:07:48', '2017-06-12 00:07:48'),
(81, 80, 'product_image\\cream-punjabi-wedding-sherwani-in-jacquard-h15283-411.jpg', '2017-06-13 00:04:52', '2017-06-13 00:04:52'),
(82, 80, 'product_image\\download (2).jpg', '2017-06-13 00:04:52', '2017-06-13 00:04:52');

-- --------------------------------------------------------

--
-- Table structure for table `product_sizes`
--

CREATE TABLE `product_sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_sizes`
--

INSERT INTO `product_sizes` (`id`, `product_id`, `size`, `created_at`, `updated_at`) VALUES
(1, 79, 'XL', '2017-06-18 00:34:28', '2017-06-18 00:34:28'),
(3, 72, 'L', '2017-06-18 00:48:54', '2017-06-18 00:48:54'),
(4, 72, 'M', '2017-06-18 00:48:59', '2017-06-18 00:48:59'),
(5, 72, 'S', '2017-06-18 00:49:05', '2017-06-18 00:49:05');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_addresses`
--

CREATE TABLE `shipping_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shipping_addresses`
--

INSERT INTO `shipping_addresses` (`id`, `order_number`, `name`, `phone`, `address`, `location`, `created_at`, `updated_at`) VALUES
(1, '1165', 'Al Mahmud ', '01675646860', 'Dhaka', 'Inside Dhaka', '2017-01-11 06:43:49', '2017-01-11 06:43:49'),
(2, '5698', 'Mahmud Hira', '01675646868', 'Mymenshing', 'OutSide Dhaka', '2017-01-11 06:47:06', '2017-01-11 06:47:06'),
(3, '17222', 'Sabbir', '01675646860', 'Dhaka', 'Inside Dhaka', '2017-01-12 07:12:44', '2017-01-12 07:12:44'),
(4, '26432', 'mahmud', '01712763416', 'Dhaka', '', '2017-06-19 02:32:55', '2017-06-19 02:32:55'),
(5, '9849', 'NIlim', '01712763416', 'Dhaka , bangladesh', '100', '2017-06-19 02:40:18', '2017-06-19 02:40:18'),
(6, '28606', 'Hira', '32166526526', 'Nara', '95', '2017-06-19 02:43:53', '2017-06-19 02:43:53');

-- --------------------------------------------------------

--
-- Table structure for table `slider_images`
--

CREATE TABLE `slider_images` (
  `slider_image_id` int(10) UNSIGNED NOT NULL,
  `slider_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slider_images`
--

INSERT INTO `slider_images` (`slider_image_id`, `slider_image`, `publication_status`, `created_at`, `updated_at`) VALUES
(18, 'slider_image\\download (1).jpg', 1, '2017-06-07 03:58:19', '2017-06-07 03:58:19'),
(19, 'slider_image\\download.jpg', 1, '2017-06-07 03:58:19', '2017-06-07 03:58:19'),
(20, 'slider_image\\images.jpg', 1, '2017-06-07 03:58:19', '2017-06-07 03:58:19');

-- --------------------------------------------------------

--
-- Table structure for table `subscribes`
--

CREATE TABLE `subscribes` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subscribes`
--

INSERT INTO `subscribes` (`id`, `email`, `created_at`, `updated_at`) VALUES
(1, 'mahmud@gmail.com', '2017-05-15 05:22:21', '2017-05-15 05:22:21'),
(2, 'mahmud.agvbd@gmail.com', '2017-05-15 05:24:41', '2017-05-15 05:24:41');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `sub_category_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub_category_name_bn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`sub_category_id`, `category_id`, `sub_category_name`, `sub_category_name_bn`, `publication_status`, `created_at`, `updated_at`) VALUES
(34, 33, 'MEN\'S CLOTHING', 'মেন্স ক্লোথিং', 1, '2017-06-07 22:39:25', '2017-06-07 22:39:25'),
(35, 33, 'TRADITIONAL CLOTHING', 'ট্রাডিশনাল ক্লোথিং', 1, '2017-06-07 22:43:19', '2017-06-07 22:43:19'),
(36, 33, 'MEN\'S ACCESSORIES', 'মেন্স একসেসোরিজ', 1, '2017-06-07 22:43:57', '2017-06-07 22:43:57'),
(37, 33, 'MEN\'S SHOES', 'মেন্স সুস ', 1, '2017-06-07 22:47:31', '2017-06-07 22:47:31'),
(38, 33, 'INNERWEAR & NIGHTWEAR', 'ইননেরওয়ার  & নিঘ্ত্বের ', 1, '2017-06-07 22:48:07', '2017-06-07 22:48:07'),
(39, 33, 'WINTER CLOTHING', 'উইন্টার ক্লোথিং', 1, '2017-06-07 22:50:17', '2017-06-07 22:50:17'),
(40, 33, 'MEN\'S WATCHES', 'মেন্স ওয়াচেস ', 1, '2017-06-07 22:51:16', '2017-06-07 22:51:16');

-- --------------------------------------------------------

--
-- Table structure for table `sub_sub_categories`
--

CREATE TABLE `sub_sub_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `sub_sub_category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub_sub_category_name_bn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_sub_categories`
--

INSERT INTO `sub_sub_categories` (`id`, `sub_category_id`, `sub_sub_category_name`, `sub_sub_category_name_bn`, `publication_status`, `created_at`, `updated_at`) VALUES
(9, 34, 'T-Shirts', 'টি -শার্ট', 1, '2017-06-07 23:33:38', '2017-06-07 23:33:38'),
(10, 34, 'Polo’s', 'পোলো’স', 1, '2017-06-07 23:38:40', '2017-06-07 23:38:40'),
(11, 34, 'Shirt', 'শার্ট ', 1, '2017-06-07 23:49:07', '2017-06-07 23:49:07'),
(12, 34, 'Coats & Jackets', 'কোটস ও জ্যাকেট', 1, '2017-06-07 23:52:11', '2017-06-07 23:52:11'),
(13, 34, 'Pants', 'প্যান্টস', 1, '2017-06-07 23:59:25', '2017-06-07 23:59:25'),
(14, 34, 'Jeans', 'জিন্স', 1, '2017-06-08 00:00:39', '2017-06-08 00:00:39'),
(15, 34, 'Shorts & Barmudas', 'শর্টস & বারমুডা', 1, '2017-06-08 00:02:04', '2017-06-08 00:04:37'),
(16, 35, 'Panjabis & Sherwanis', 'পাঞ্জাবি ও শেরওয়ানি', 1, '2017-06-13 00:01:40', '2017-06-13 00:01:40');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mahmud Hira', 'mahmud@gmail.com', '$2y$10$3nzi3/N/s/1KcWR6eZAvb.tuWpu6mFBjTH90YY3DVYxdGU8YZsl3W', 'r6uYBulIONuSZgkSu0JhTrfqbd3uvjVlQIyIKuvju04NWExNVbX6pzb0dduO', NULL, '2017-06-19 02:48:36');

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wishlists`
--

INSERT INTO `wishlists` (`id`, `customer_id`, `product_id`, `created_at`, `updated_at`) VALUES
(5, 1, 56, '2017-01-10 00:26:19', '2017-01-10 00:26:19'),
(7, 1, 55, '2017-01-10 05:05:20', '2017-01-10 05:05:20'),
(9, 1, 54, '2017-01-10 05:06:46', '2017-01-10 05:06:46'),
(10, 1, 52, '2017-01-10 05:10:47', '2017-01-10 05:10:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `add_to_carts`
--
ALTER TABLE `add_to_carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `pazzles`
--
ALTER TABLE `pazzles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`product_image_id`);

--
-- Indexes for table `product_sizes`
--
ALTER TABLE `product_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_addresses`
--
ALTER TABLE `shipping_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_images`
--
ALTER TABLE `slider_images`
  ADD PRIMARY KEY (`slider_image_id`);

--
-- Indexes for table `subscribes`
--
ALTER TABLE `subscribes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`sub_category_id`);

--
-- Indexes for table `sub_sub_categories`
--
ALTER TABLE `sub_sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `add_to_carts`
--
ALTER TABLE `add_to_carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pazzles`
--
ALTER TABLE `pazzles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `product_image_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `product_sizes`
--
ALTER TABLE `product_sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `shipping_addresses`
--
ALTER TABLE `shipping_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `slider_images`
--
ALTER TABLE `slider_images`
  MODIFY `slider_image_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `subscribes`
--
ALTER TABLE `subscribes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `sub_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `sub_sub_categories`
--
ALTER TABLE `sub_sub_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
